using Godot;
using System;

namespace ProtoVania
{
    public partial class DummyController : Area2D
    {
        [Export] public int damage = 10;

        public override void _Ready() => BodyEntered += DoDamage;

        void DoDamage(Node2D body)
        {
            if (body.IsInGroup("Player"))
            {
                var script = Utils.GetNode<PlayerHealth>(body);
                script.TakeDamage(damage);
            }
        }

    }
}