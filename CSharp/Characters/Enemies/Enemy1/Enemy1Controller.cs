using Godot;
using System;

namespace ProtoVania
{
    public partial class Enemy1Controller : CharacterBody2D, IStateMachine
    {
        public IState CurrentState { get; private set; } = null;

        [Export] public float _movementBaseSpeed = 0f;
        public float MovementBaseSpeed { get { return _movementBaseSpeed; } }

        private AnimatedSprite2D _animatedSprite = null;
        public AnimatedSprite2D AnimatedSprite { get { return _animatedSprite; } }

        public float MovementSpeed { get; set; } = 0f;

        public int FaceDirection { get; set; } = -1;

        public override void _Ready()
        {
            InitializeVariables();
            ChangeState(new Enemy1IdleState());
        }

        public override void _Process(double delta)
        {
            Flip();
            if (CurrentState == null)
                _Ready();
            if (CurrentState is IProcessState state)
                state.Process(delta);
        }

        public override void _PhysicsProcess(double delta)
        {
            if (CurrentState == null)
                _Ready();
            if (CurrentState is IPhysicsProcessState state)
                state.PhysicsProcess(delta);
        }

        void InitializeVariables()
        {
            _animatedSprite = GetNode("AnimatedSprite") as AnimatedSprite2D;
            MovementSpeed = MovementBaseSpeed;
        }

        public void ChangeState(IState state)
        {
            (CurrentState as Node2D)?.QueueFree();
            CurrentState = state;
            CurrentState.EnterState(this);
        }

        public void MoveHorizontal(double delta, bool isIdle = false)
        {
            Velocity += new Vector2(Velocity.X, Utils.ToSingle(Constants.Gravity * delta));
            if (!isIdle)
                Velocity = new Vector2(MovementSpeed * FaceDirection, Velocity.Y);
            MoveAndSlide();
        }


        public int GetRandomDirection()
        {
            var rdm = new Random();
            var direction = rdm.Next(-1, 2);
            if (direction == 0)
                direction = GetRandomDirection();
            return direction;
        }

        public void Flip()
        {
            if (FaceDirection == -1)
                AnimatedSprite.FlipH = false;
            else
                AnimatedSprite.FlipH = true;

        }
    }
}
