using Godot;
using System;

namespace ProtoVania
{
    public class Enemy1IdleState : IState, IProcessState
    {
        public string StateName { get; private set; } = "Enemy1Idle";
        Enemy1Controller Enemy1;

        private double _stateDuration = 0f;

        public void EnterState(Node2D controller)
        {
            Enemy1 = controller as Enemy1Controller;
            var rdm = new Random();
            _stateDuration = rdm.Next(1, 4);
            Enemy1.FaceDirection = Enemy1.GetRandomDirection();
            Enemy1.Velocity = Vector2.Zero;
        }

        public void Process(double delta)
        {
            DurationTick(delta);
            if (_stateDuration <= 0)
                ExitState();
            else
                Enemy1.MoveHorizontal(delta, true);
        }

        void DurationTick(double delta)
        {
            if (_stateDuration > 0)
                _stateDuration -= delta;
        }

        public void ExitState() => Enemy1.ChangeState(new Enemy1MoveState());
    }
}
