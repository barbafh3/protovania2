using Godot;
using System;

namespace ProtoVania
{
    public partial class Enemy1MoveState : Node2D, IState, IProcessState
    {
        public string StateName { get { return "Enemy1Move"; } }
        Enemy1Controller Enemy1;

        private double _stateDuration = 0f;

        public void EnterState(Node2D controller)
        {
            Enemy1 = controller as Enemy1Controller;
            var rdm = new Random();
            _stateDuration = rdm.Next(1, 4);
            PlayAnimation();
        }

        public void Process(double delta)
        {
            DurationTick(delta);
            if (_stateDuration <= 0)
                ExitState();
            else
                Enemy1.MoveHorizontal(delta);

        }

        private void DurationTick(double delta)
        {
            if (_stateDuration > 0)
                _stateDuration -= delta;
        }

        private void PlayAnimation()
        {
            if (Enemy1.FaceDirection == -1)
                Enemy1.AnimatedSprite.FlipH = false;
            if (Enemy1.FaceDirection == 1)
                Enemy1.AnimatedSprite.FlipH = true;
            Enemy1.AnimatedSprite.Play("Move");
        }

        public void ExitState() => Enemy1.ChangeState(new Enemy1IdleState());

    }
}
