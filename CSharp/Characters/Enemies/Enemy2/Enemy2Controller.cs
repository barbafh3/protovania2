using Godot;
using System;

namespace ProtoVania
{
    public partial class Enemy2Controller : CharacterBody2D, IStateMachine, IMobile
    {
        public IState CurrentState { get; private set; } = null;

        [Export] private float _baseMovementSpeed = 0f;
        public float BaseMovementSpeed { get { return _baseMovementSpeed; } }

        [Export] private float _turningBaseTimer = 0f;
        public float BaseTurningTimer { get { return _turningBaseTimer; } }

        public AnimatedSprite2D Enemy2AnimatedSprite { get; private set; } = null;
        public Sprite2D Enemy2Sprite { get; private set; } = null;
        public PlayerController Player { get; set; } = null;

        public double MovementSpeed { get; set; } = 0f;
        public double TurningTimer { get; set; } = 0f;

        public int FaceDirection { get; set; } = -1;

        public override void _Ready()
        {
            InitializeVariables();
            ChangeState(new Enemy2IdleState());
        }

        public override void _Process(double delta)
        {
            Flip();
            if (CurrentState == null)
                _Ready();
            if (CurrentState is IProcessState)
                ((IProcessState)CurrentState).Process(delta);
        }

        public override void _PhysicsProcess(double delta)
        {
            if (CurrentState == null)
                _Ready();
            if (CurrentState is IPhysicsProcessState)
                ((IPhysicsProcessState)CurrentState).PhysicsProcess(delta);
        }

        void InitializeVariables()
        {
            Enemy2AnimatedSprite = GetNode("AnimatedSprite") as AnimatedSprite2D;
            Enemy2Sprite = GetNode("Sprite") as Sprite2D;
            Enemy2Sprite.Visible = false;
            MovementSpeed = BaseMovementSpeed;
        }

        public void ChangeState(IState state)
        {
            if (CurrentState is Node2D) ((Node2D)CurrentState).QueueFree();
            CurrentState = state;
            CurrentState.EnterState(this);
            GD.Print("Enemy1: State = " + CurrentState.StateName);
        }

        public void MoveHorizontal(double delta, double jumpForce = 0, double forcedH = 0, bool isOnWallAir = false)
        {
            if (Player != null)
            {
                if (GlobalPosition > Player.GlobalPosition)
                    FaceDirection = -1;
                else
                    FaceDirection = 1;
            }
            Velocity += new Vector2(Velocity.X, Utils.ToSingle(Constants.Gravity * delta));
            Velocity = new Vector2(Utils.ToSingle(MovementSpeed * FaceDirection), Velocity.Y);
            MoveAndSlide();
        }

        public void MoveVertical(Node2D controller, float delta, float jumpForce = 0, bool lockX = false)
        {
        }

        public int GetRandomDirection()
        {
            var rdm = new Random();
            var direction = rdm.Next(-1, 2);
            if (direction == 0)
                direction = GetRandomDirection();
            return direction;
        }

        public void Flip()
        {
            if (FaceDirection == -1)
                Enemy2AnimatedSprite.FlipH = false;
            else
                Enemy2AnimatedSprite.FlipH = true;

        }
    }
}
