using Godot;

namespace ProtoVania
{
    public class Enemy2IdleState : IState, IProcessState
    {
        public string StateName { get { return "Enemy2Idle"; } }
        Enemy2Controller enemy2;

        public void EnterState(Node2D controller)
        {
            enemy2 = controller as Enemy2Controller;
        }

        public void Process(double delta)
        {
            TurningTimerTick(delta);
            if (enemy2.TurningTimer <= 0)
            {
                enemy2.FaceDirection = enemy2.GetRandomDirection();
                enemy2.TurningTimer = enemy2.BaseTurningTimer;
            }
            enemy2.MoveHorizontal(delta);
        }

        void TurningTimerTick(double delta)
        {
            if (enemy2.TurningTimer > 0)
                enemy2.TurningTimer -= delta;
        }

        public void ExitState() { }
    }
}
