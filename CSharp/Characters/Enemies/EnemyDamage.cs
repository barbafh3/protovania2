using Godot;

namespace ProtoVania
{
    public partial class EnemyDamage : Area2D, IDamaging
    {

        [Export] public int baseDamage = 0;
        public int damage { get; set; } = 0;

        public override void _Ready()
        {
            BodyEntered += DoDamage;
            damage = baseDamage;
        }

        public void DoDamage(Node2D body)
        {
            if (body.IsInGroup("Player"))
            {
                var script = Utils.GetNode<PlayerHealth>(body);
                script.TakeDamage(damage);
            }
        }

    }
}
