using Godot;

namespace ProtoVania
{
    public partial class EnemyHealth : Node2D, IDamageable
    {
        [Export] public int maxHealth = 0;
        [Export] public int startingHealth = 0;
        public int currentHealth = 0;

        public override void _Ready() => currentHealth = startingHealth;

        public override void _Process(double delta)
        {
            if (currentHealth <= 0)
                Die();
        }

        public void TakeDamage(int amount)
        {
            var resultingHealth = currentHealth - amount;
            if (resultingHealth <= 0)
                currentHealth = 0;
            else
                currentHealth = resultingHealth;
        }

        public void Die() => GetParent().QueueFree();
    }
}
