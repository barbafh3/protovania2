using Godot;

namespace ProtoVania
{
    public partial class PlayerDetection : Area2D
    {

        Enemy2Controller parent;

        public override void _Ready()
        {
            BodyEntered += CheckIfPlayer;
            BodyExited += ClearPlayerReference;
            parent = GetParent() as Enemy2Controller;
        }

        public override void _Process(double delta)
        {
            if (parent.Player != null)
            {
                if (!parent.Player.IsInGroup("PlayerDetection"))
                {
                    parent.Enemy2Sprite.Visible = false;
                    parent.Player = null;
                }
            }
            if (GetOverlappingBodies().Count > 0)
            {
                foreach (var body in GetOverlappingBodies())
                    CheckIfPlayer(body);
            }
        }

        void CheckIfPlayer(Node2D body)
        {
            if (body.IsInGroup("PlayerDetection"))
            {
                parent.Player = body as PlayerController;
                parent.Enemy2Sprite.Visible = true;
            }
        }

        void ClearPlayerReference(Node2D body)
        {
            if (body.IsInGroup("PlayerDetection"))
            {
                parent.Enemy2Sprite.Visible = false;
                parent.Player = null;
            }
        }

    }
}
