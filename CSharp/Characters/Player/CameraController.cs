using Godot;
using System;

namespace ProtoVania
{
    public partial class CameraController : Camera2D, IStateMachine
    {
        public IState CurrentState { get; private set; } = null;

        public float ShakePower { get; set; }
        public float ShakeDuration { get; set; }
        public bool IsShaking { get; private set; } = false;
        public Vector2 CurrentPosition { get; private set; }
        public float ElapsedTime { get; set; }

        public override void _Ready()
        {
            InitializeVariables();
            ChangeState(new CameraDefaultState());
        }

        public override void _Process(double delta)
        {
            if (CurrentState == null)
                _Ready();
            if (CurrentState is IProcessState)
                ((IProcessState)CurrentState).Process(delta);
            if (IsShaking) Shake((float)delta);
        }

        public override void _PhysicsProcess(double delta)
        {
            if (CurrentState == null)
                _Ready();
            if (CurrentState is IPhysicsProcessState)
                ((IPhysicsProcessState)CurrentState).PhysicsProcess(delta);
        }

        public void InitializeVariables()
        {
            CurrentPosition = Offset;
        }

        public void ChangeState(IState state)
        {
            if (CurrentState is Node2D) ((Node2D)CurrentState).QueueFree();
            CurrentState = state;
            CurrentState.EnterState(this);
        }

        void StartShake(float power, float duration)
        {
            ElapsedTime = 0f;
            ShakePower = power;
            ShakeDuration = duration;
            IsShaking = true;
        }

        void Shake(float delta)
        {
            if (ElapsedTime < ShakeDuration)
            {
                Offset = new Vector2((float)new Random().NextDouble(), (float)new Random().NextDouble()) * ShakePower;
                ElapsedTime += delta;
            }
            else
            {
                IsShaking = false;
                ElapsedTime = 0;
                Offset = CurrentPosition;
            }
        }
    }
}