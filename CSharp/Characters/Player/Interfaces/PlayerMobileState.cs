using Godot;

namespace ProtoVania
{
    public abstract partial class PlayerMobileState : Node2D, IMobile
    {
        protected PlayerController Player;
        public void MoveHorizontal(Node2D controller, float delta, float jumpForce = 0f, float forcedH = 0f, bool isOnWallAir = false)
        {
            var vel = Player.Velocity;
            
            if (forcedH != 0f)
            {
                vel += new Vector2(
                        (float)(forcedH * Player.RunSpeed),
                        (float)(Constants.Gravity * Player.WallJumpGravityModifier * delta)
                    );
            }
            else
            {
                if (isOnWallAir)
                    vel.Y = (float)(Constants.Gravity * Player.WallFallGravityModifier * delta);
                else
                    vel.Y += (float)(Constants.Gravity * delta);
                var horizontal = Input.GetActionStrength("move_right") - Input.GetActionStrength("move_left");
                vel.X = (float)(horizontal * Player.RunSpeed);
            }
            if (jumpForce != 0f)
                vel.Y = jumpForce;
            
            Player.Velocity = vel;
            Player.MoveAndSlide();
        }

        public void MoveVertical(Node2D controller, double delta, float jumpForce = 0f, bool lockX = false)
        {
            Vector2 vel = Player.Velocity;
            
            if (lockX) vel.X = 0f;
            var vertical = Input.GetActionStrength("move_down") - Input.GetActionStrength("move_up");
            var modifiedGravity = Constants.Gravity * Player.WallFallGravityModifier * delta;
            if (vertical != 0)
                vel.Y = (float)(vertical * Player.ClimbSpeed + modifiedGravity);
            else
                vel.Y = (float)modifiedGravity;
            if (jumpForce != 0.0)
                vel.X = jumpForce;
            
            Player.Velocity = vel;
            Player.MoveAndSlide();
        }
    }
}
