using Godot;

namespace ProtoVania.CSharp.Characters.Player.Interfaces;

public partial class PlayerSkill : Node2D
{
    [Signal] public delegate void SkillUsedEventHandler();
    [Signal] public delegate void SkillEndedEventHandler();
    
    [Export] protected float _baseCooldown = 0f;
    public float BaseCooldown => _baseCooldown;
    
    public float Cooldown { get; set; } = 0f;
}