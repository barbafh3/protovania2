using Godot;

namespace ProtoVania
{
    public partial class PetAttackState : Node2D, IState, IPhysicsProcessState
    {
        public string StateName { get; private set; } = "PetAttack";
        PetController Pet;
        private Node2D Target;

        public void EnterState(Node2D controller)
        {
            Pet = controller as PetController;
            Pet.Collision.BodyEntered += BodyCollision;
            Pet.Collision.AreaEntered += AreaCollision;
            Target = FindNearestEnemy();
            GD.Print("Pet target position found: " + Target?.GlobalPosition);
            if (Target == null)
                ExitState();
        }

        public void PhysicsProcess(double delta)
        {
            if (Target != null && (Target.GlobalPosition - Pet.GlobalPosition).Length() > 5f)
            {
                var vector = (Target.GlobalPosition - Pet.GlobalPosition).Normalized();
                var velocity = Pet.attackMoveSpeed * vector * (float)delta;
                Pet.MoveAndCollide(velocity);
            }
            else
                ExitState();
        }

        Node2D FindNearestEnemy()
        {
            Node2D selectedEnemyPosition = null;
            float smallestDistance = float.PositiveInfinity;

            var detectedBodies = Pet.Detection.GetOverlappingBodies();
            foreach (Node2D body in detectedBodies)
            {
                if (body.IsInGroup("Enemy"))
                {
                    var distance = Pet.GlobalPosition.DistanceTo(body.GlobalPosition);
                    if (distance < smallestDistance)
                    {
                        selectedEnemyPosition = body;
                        smallestDistance = distance;
                    }
                }
            }

            var detectedAreas = Pet.Detection.GetOverlappingAreas();
            foreach (Node2D area in detectedAreas)
            {
                if (area.IsInGroup("Enemy"))
                {
                    var distance = Pet.GlobalPosition.DistanceTo(area.GlobalPosition);
                    if (distance < smallestDistance)
                    {
                        selectedEnemyPosition = area;
                        smallestDistance = distance;
                    }
                }
            }
            return selectedEnemyPosition;
        }

        void BodyCollision(Node2D body) { if (body.Equals(Target)) DoDamage(body); }

        void AreaCollision(Node2D area) { if (area.Equals(Target)) DoDamage(area); }

        void DoDamage(Node2D node)
        {
            Target = null;
            Utils.GetNode<IDamageable>(node).TakeDamage(Pet.damage);
        }

        public void ExitState() => Pet.ChangeState(new PetIdleState());
    }
}
