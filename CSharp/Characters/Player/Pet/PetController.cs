using Godot;
using ProtoVania.CSharp.Characters.Player.Interfaces;

namespace ProtoVania
{
    public partial class PetController : CharacterBody2D, IStateMachine
    {

        public IState CurrentState { get; set; }

        // Node references
        private PlayerController _player = null;
        
        public PlayerSkill SkillNode;

        public PlayerController Player => _player;

        private Area2D _detection = null;

        public Area2D Detection => _detection;

        private Area2D _collision = null;

        public Area2D Collision => _collision;

        private AnimatedSprite2D _animatedSprite = null;

        public AnimatedSprite2D PetSprite => _animatedSprite;

        // Position variables
        [Export] public float yOffset = 0f;
        [Export] public float xOffset = 0f;

        private Vector2 _leftPosition = Vector2.Zero;

        public Vector2 LeftPosition => _leftPosition;

        private Vector2 _rightPosition = Vector2.Zero;

        public Vector2 RightPosition => _rightPosition;

        // Movement variables
        [Export] public float moveSpeed = 0f;

        // Attack variables
        [Export] public float attackMoveSpeed = 0f;
        [Export] public int damage = 0;
        [Export] private float _baseCooldown = 0f;

        public float BaseCooldown => _baseCooldown;

        public float Cooldown { get; set; } = 0f;
        public Vector2 targetPosition;

        public override void _Ready()
        {
            InitializeVariables();
            ConnectSignals();
            ChangeState(new PetIdleState());
        }

        public override void _Process(double delta)
        {
            Flip();
            if (Input.IsActionJustReleased("pet"))
                SkillNode.EmitSignal(PlayerSkill.SignalName.SkillUsed);
            if (CurrentState == null)
                _Ready();
            if (CurrentState is IProcessState)
                ((IProcessState)CurrentState).Process(delta);
        }

        public override void _PhysicsProcess(double delta)
        {
            if (CurrentState == null)
                _Ready();
            if (CurrentState is IPhysicsProcessState)
                ((IPhysicsProcessState)CurrentState).PhysicsProcess(delta);
        }

        void InitializeVariables()
        {
            _player = GetTree().Root.GetNode("Map/Characters/Player") as PlayerController;
            SkillNode = _player.GetNode<PlayerSkill>("PlayerPet");
            _animatedSprite = GetNode("AnimatedSprite") as AnimatedSprite2D;
            _leftPosition = new Vector2(-xOffset, yOffset);
            _rightPosition = new Vector2(xOffset, yOffset);
            Position = new Vector2(Player.GlobalPosition.X + xOffset, Player.GlobalPosition.Y + yOffset);
            _detection = GetNode("Detection") as Area2D;
            _collision = GetNode("Collision") as Area2D;
        }

        void ConnectSignals() => Player.Flipped += Flip;

        public void ChangeState(IState state)
        {
            if (CurrentState != null)
                (CurrentState as Node2D)?.QueueFree();
            CurrentState = state;
            CurrentState.EnterState(this);
        }

        public void Flip()
        {
            if (Player != null)
            {
                if (Player.FaceDirection == 1)
                {
                    PetSprite.FlipH = true;
                    targetPosition = Player.GlobalPosition + LeftPosition;
                }

                if (Player.FaceDirection == -1)
                {
                    PetSprite.FlipH = false;
                    targetPosition = Player.GlobalPosition + RightPosition;
                }
            }
        }
    }
}