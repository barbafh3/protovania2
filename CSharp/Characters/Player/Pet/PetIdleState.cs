using Godot;

namespace ProtoVania
{
    public partial class PetIdleState : Node2D, IState, IProcessState
    {
        public string StateName
        {
            get { return "PetIdle"; }
        }

        PetController Pet;

        public void EnterState(Node2D controller)
        {
            Pet = controller as PetController;
            Pet.SkillNode.SkillUsed += ExitState;
        }

        public void Process(double delta)
        {
            PetCooldown((float)delta);
            if ((Pet.targetPosition - Pet.GlobalPosition).Length() > 2)
                Move((float)delta);
            else
                ForceFlip(Pet.Player.FaceDirection);
        }

        private void PetCooldown(float delta)
        {
            if (Pet.Cooldown > 0)
                Pet.Cooldown -= delta;
        }

        private void Move(float delta)
        {
            var vector = (Pet.targetPosition - Pet.GlobalPosition).Normalized();
            ForceFlip(vector.X);
            Vector2 velocity = Pet.moveSpeed * vector * delta;
            Pet.MoveAndCollide(velocity);
        }

        void ForceFlip(float direction)
        {
            if (direction > 0)
                Pet.PetSprite.FlipH = true;
            else if (direction < 0)
                Pet.PetSprite.FlipH = false;
        }

        public void ExitState() => Pet.ChangeState(new PetAttackState());
    }
}