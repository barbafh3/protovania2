using Godot;
using ProtoVania.CSharp.Characters.Player.Interfaces;

namespace ProtoVania
{
    public partial class PlayerBarrier : PlayerSkill
    {
        PlayerController Player;


        [Export] private float _baseDuration = 0f;
        public float BaseDuration => _baseDuration;

        public float Duration { get; set; } = 0f;

        public override void _Ready()
        {
            Player = GetParent<PlayerController>();
            SkillEnded += DisableBarrier;
            Cooldown = BaseCooldown;
        }

        public override void _Process(double delta)
        {
            if (Input.IsActionJustPressed("barrier"))
                ActivateBarrier();
        }

        void CooldownTick(float delta)
        {
            if (Cooldown > 0)
                Cooldown -= delta;
        }

        void DurationTick(float delta)
        {
            if (Duration > 0)
            {
                Duration -= delta;
                if (Duration <= 0)
                {
                    EmitSignal(PlayerSkill.SignalName.SkillEnded);
                    GD.Print("Player: Barrier ended");
                }
            }
        }

        public void ActivateBarrier()
        {
            Player.Barrier.Visible = true;
            Player.CanTakeDamage = false;
            Cooldown = BaseCooldown;
            Duration = BaseDuration;
        }

        void DisableBarrier()
        {
            Player.Barrier.Visible = false;
            Player.CanTakeDamage = true;
        }
    }
}