using Godot;
using ProtoVania.CSharp.Characters.Player.Interfaces;

namespace ProtoVania
{
    public partial class PlayerCamouflage : PlayerSkill
    {
        [Export] private float _baseDuration = 0f;
        public float BaseDuration => _baseDuration;
        
        PlayerController Player;

        public float Duration { get; set; } = 0f;

        public override void _Ready()
        {
            SkillEnded += DisableCamouflage;
            Cooldown = BaseCooldown;
        }

        public override void _Process(double delta)
        {
            CooldownTick((float)delta);
            DurationTick((float)delta);
            if (Input.IsActionJustPressed("camouflage"))
                ActivateCamouflage((float)delta);
        }

        void CooldownTick(float delta)
        {
            if (Cooldown > 0f)
                Cooldown -= delta;
        }

        void DurationTick(float delta)
        {
            if (Duration > 0)
            {
                Duration -= delta;
                if (Duration <= 0)
                {
                    EmitSignal(PlayerSkill.SignalName.SkillEnded);
                }
            }
        }

        public void ActivateCamouflage(float delta)
        {
            Duration = BaseDuration;
            Cooldown = BaseCooldown;
            var newModulate = Player.AnimatedSprite.Modulate;
            newModulate.A = 0.5f;
            Player.AnimatedSprite.Modulate = newModulate;
            RemoveFromGroup("PlayerDetection");
        }

        void DisableCamouflage()
        {
            var newModulate = Player.AnimatedSprite.Modulate;
            newModulate.A = 1f;
            Player.AnimatedSprite.Modulate = newModulate;
            AddToGroup("PlayerDetection");
        }
    }
}