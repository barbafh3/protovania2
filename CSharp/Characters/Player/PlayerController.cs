using System;
using Godot;

namespace ProtoVania
{
    public partial class PlayerController : CharacterBody2D, IStateMachine
    {
        [Signal] public delegate void FlippedEventHandler();

        public Type LastStateType { get; private set; } = null;
        public IState CurrentState { get; private set; } = null;
        
        public TileMap Platforms { get; set; }

        public InventoryUI InventoryUI { get; private set; }

        // Enables/disables individual skills
        [Export] private bool _canRun = true;
        public bool CanRun { get { return _canRun; } set { _canRun = value; } }
        [Export] private bool _canJump = true;
        public bool CanJump { get { return _canJump; } set { _canJump = value; } }
        [Export] private bool _canDash;
        public bool CanDash { get { return _canDash; } set { _canDash = value; } }
        [Export] private bool _canClimb = false;
        public bool CanClimb { get { return _canClimb; } set { _canClimb = value; } }
        [Export] private bool _canFire = false;
        public bool CanFire { get { return _canFire; } set { _canFire = value; } }
        [Export] private bool _canTakeDamage = true;
        public bool CanTakeDamage { get { return _canTakeDamage; } set { _canTakeDamage = value; } }

        // General use variables
        // public Vector2 StartingPosition { get { return new Vector2(1427, 489); } }
        public PlayerHealth HealthScript { get; private set; }
        public PlayerFire FireScript { get; private set; }
        public Node Root { get { return GetTree().Root; } }
        public AnimatedSprite2D AnimatedSprite { get; private set; } = null;
        public int FaceDirection { get; set; } = -1;
        public string NextAnimation { get; private set; } = "IdleLeft";
        public Timer GhostTimer { get; private set; } = null;

        // Barrier variables
        public PlayerBarrier Barrier { get; private set; }

        // Run State variables
        [Export] private float _runSpeed = 250f;
        public double RunSpeed { get { return _runSpeed; } }

        // Jump State variables
        [Export] private float _jumpForce = -650;
        public float JumpForce { get { return _jumpForce; } }
        [Export] private float _wallJumpGravityModifier = 0.8f;
        public float WallJumpGravityModifier { get { return _wallJumpGravityModifier; } }
        [Export] private int _baseJumpCount = 1;
        public int BaseJumpCount { get { return _baseJumpCount; } }
        public int JumpCount { get; set; } = 0;
        public RayCast2D GroundChecker { get; private set; } = null;
        public RayCast2D GroundChecker2 { get; private set; } = null;

        // Dash State variables
        [Export] private bool _mouseDirectionEnabled = false;
        public bool MouseDirectionEnabled { get { return _mouseDirectionEnabled; } }
        [Export] private float _dashSpeed = 18f;
        public float DashSpeed { get { return _dashSpeed; } }
        [Export] private float _dashDelay = 0.2f;
        public float DashDelay { get { return _dashDelay; } }
        [Export] private float _baseDashDuration = 0.2f;
        public float BaseDashDuration { get { return _baseDashDuration; } }
        [Export] private float _baseDashCooldown = 0.5f;
        public float BaseDashCooldown { get { return _baseDashCooldown; } }
        public float DashDuration { get; set; }
        public double DashCooldown { get; set; }
        public PackedScene GhostScene { get; private set; }

        // Climb State variables
        [Export] private float _climbSpeed = 300f;
        public float ClimbSpeed { get { return _climbSpeed; } }
        [Export] private float _wallFallGravityModifier = 3f;
        public float WallFallGravityModifier { get { return _wallFallGravityModifier; } }
        public RayCast2D LeftWallChecker { get; private set; }
        public RayCast2D LeftLedgeChecker { get; private set; }
        public RayCast2D RightWallChecker { get; private set; }
        public RayCast2D RightLedgeChecker { get; private set; }

        // Fire State variables
        [Export] public float _mouseFlipDeadzone = 0.5f;
        public float MouseFlipDeadzone { get { return _mouseFlipDeadzone; } }
        public Camera2D Camera { get; private set; }
        public Node2D Crosshair { get; private set; }

        public AudioStreamPlayer2D audioPlayer { get; private set; }
        public dynamic Sounds { get; private set; } = new
        {
            jump = ResourceLoader.Load("res://Assets/Sounds/SFX/Characters/Player/PlayerJump.wav") as AudioStream,
            dash = ResourceLoader.Load("res://Assets/Sounds/SFX/Characters/Player/PlayerDash.wav") as AudioStream,
            land = ResourceLoader.Load("res://Assets/Sounds/SFX/Characters/Player/PlayerLand.wav") as AudioStream,
            melee = ResourceLoader.Load("res://Assets/Sounds/SFX/Characters/Player/PlayerAttack.wav") as AudioStream,
            ranged = ResourceLoader.Load("res://Assets/Sounds/SFX/Characters/Player/PlayerProjectileLaunch.wav") as AudioStream,
            damage = ResourceLoader.Load("res://Assets/Sounds/SFX/Characters/Player/PlayerDamage.wav") as AudioStream,
        };

        public PlayerInventory Inventory { get; private set; }

        public bool IsOnLedge { get; set; } = false;

        public string[] AwaitAnimations { get; private set; } = { "FireLeft", "FireRight", "LandLeft", "LandRight" };

        public override void _Ready()
        {
            InitializeVariables();
            ConnectSignals();
            ChangeState(new PlayerIdleState());
        }

        public override void _Process(double delta)
        {
            CooldownTick(delta);
            if (CurrentState == null)
                ChangeState(new PlayerIdleState());
            if (CurrentState is IProcessState state)
                state.Process(delta);
        }

        public override void _PhysicsProcess(double delta)
        {
            if (CurrentState == null)
                ChangeState(new PlayerIdleState());
            if (CurrentState is IPhysicsProcessState state)
                state.PhysicsProcess(delta);
        }

        public void InitializeVariables()
        {
            // Position = StartingPosition;
            AnimatedSprite = GetNode("Body") as AnimatedSprite2D;
            audioPlayer = GetNode("AudioStreamPlayer2D") as AudioStreamPlayer2D;
            GroundChecker = GetNode("Ground") as RayCast2D;
            GroundChecker2 = GetNode("Ground2") as RayCast2D;
            LeftWallChecker = GetNode("LeftWall") as RayCast2D;
            LeftLedgeChecker = GetNode("LeftLedge") as RayCast2D;
            RightWallChecker = GetNode("RightWall") as RayCast2D;
            RightLedgeChecker = GetNode("RightLedge") as RayCast2D;
            Camera = GetNode("Camera2D") as Camera2D;
            GhostScene = ResourceLoader.Load("res://Scenes/Characters/Player/PlayerGhost.tscn") as PackedScene;
            GhostTimer = GetNode("GhostTimer") as Timer;
            Barrier = GetNode<PlayerBarrier>("PlayerBarrier");
            Barrier.Visible = false;
            AddToGroup("PlayerDetection");
            var canvasLayer = Utils.GetNode<CanvasLayer>(this);
            InventoryUI = Utils.GetNode<InventoryUI>(canvasLayer);
            HealthScript = Utils.GetNode<PlayerHealth>(this);
            FireScript = Utils.GetNode<PlayerFire>(this);
            SetInventory();
        }

        public void SetInventory() => Inventory = Utils.GetNode<PlayerInventory>(this);

        public void ConnectSignals()
        {
            AnimatedSprite.AnimationFinished += CheckAnimation;
        }

        public void ChangeState(IState state)
        {
            if (CurrentState != null)
                LastStateType = CurrentState.GetType();
            if (CurrentState != null) (CurrentState as Node2D)?.QueueFree();
            CurrentState = state;
            CurrentState.EnterState(this);
            GD.Print("PlayerController: State = " + CurrentState.StateName);
        }

        void CooldownTick(double delta)
        {
            if (DashCooldown > 0f)
                DashCooldown -= delta;
        }

        public void Flip()
        {
            var mousePos = GetGlobalMousePosition();
            if (mousePos.X > GlobalPosition.X + MouseFlipDeadzone && FaceDirection != 1)
            {
                FaceDirection = 1;
                FireScript.ShotOriginOffset = Mathf.Abs(FireScript.ShotOriginOffset);
                EmitSignal("Flipped");
            }
            if (mousePos.X < GlobalPosition.X - MouseFlipDeadzone && FaceDirection != -1)
            {
                FaceDirection = -1;
                FireScript.ShotOriginOffset = -Mathf.Abs(FireScript.ShotOriginOffset);
                EmitSignal("Flipped");
            }

        }

        public void CheckAnimation()
        {
            if (Array.Exists(AwaitAnimations, animation => animation == AnimatedSprite.Animation))
            {
                if (NextAnimation != "")
                {
                    AnimatedSprite.Play(NextAnimation);
                    NextAnimation = "";
                }
            }
        }

        public void SetAnimation(string leftAnimation, string rightAnimation)
        {
            NextAnimation = FaceDirection == 1 ? rightAnimation : leftAnimation;
            // if (!Array.Exists(AwaitAnimations, animation => animation == AnimatedSprite.Animation))
            // {
            if (NextAnimation != "")
            {
                AnimatedSprite.Play(NextAnimation);
                NextAnimation = "";
            }
            // }
        }

        public void GenerateGhost()
        {
            var ghost = GhostScene.Instantiate() as Node2D;
            GetTree().Root.AddChild(ghost);
            ghost.GlobalPosition = GlobalPosition;
            (ghost as Sprite2D).FlipH = AnimatedSprite.FlipH;
            (ghost as Sprite2D).Texture = AnimatedSprite.SpriteFrames.GetFrameTexture(AnimatedSprite.Animation, AnimatedSprite.Frame);
        }

    }
}
