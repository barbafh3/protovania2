using Godot;

namespace ProtoVania
{
    public partial class PlayerFire : Node2D
    {
        [Export] private float _mouseFlipDeadzone = 0.5f;
        [Export] private float _baseShotOriginOffset = 0.5f;
        [Export] private float _crosshairDistance = 0.5f;
        [Export] private float _baseCooldown = 0.5f;

        public float MouseFlipDeadzone => _mouseFlipDeadzone;
        public float BaseShotOriginOffset => _baseShotOriginOffset;

        public float CrosshairDistance => _crosshairDistance;

        public float BaseCooldown => _baseCooldown;

        public float Cooldown { get; set; } = 0f;

        PlayerController Player { get; set; }
        public PackedScene ProjectilePrev { get; set; }
        public PackedScene ChargedProjectilePrev { get; set; }
        public float ShotOriginOffset { get; set; }
        public float ChargedTime { get; set; } = 0f;

        public override void _Ready()
        {
            Player = GetParent() as PlayerController;
            ShotOriginOffset = BaseShotOriginOffset;
            ProjectilePrev = ResourceLoader.Load("res://Scenes/Projectiles/PlayerProjectile.tscn") as PackedScene;
            ChargedProjectilePrev =
                ResourceLoader.Load("res://Scenes/Projectiles/PlayerChargedProjectile.tscn") as PackedScene;
        }

        public override void _Process(double delta)
        {
            CooldownTick((float)delta);
            if (Input.IsActionPressed("fire"))
                ChargedTime += (float)delta;
            if (Input.IsActionJustReleased("fire") &&
                Player.CanFire &&
                Cooldown <= 0)
            {
                if (ChargedTime < 1f)
                    FireProjectile();
                else
                    FireChargedProjectile();
                ChargedTime = 0f;
            }
        }

        void FireProjectile()
        {
            Player.audioPlayer.Stream = Player.Sounds.ranged;
            Player.audioPlayer.Play();
            Player.SetAnimation("FireLeft", "FireRight");
            ProjectileSetup(Player, ProjectilePrev);
            Cooldown = BaseCooldown;
        }

        void FireChargedProjectile()
        {
            Player.audioPlayer.Stream = Player.Sounds.ranged;
            Player.audioPlayer.Play();
            Player.SetAnimation("FireLeft", "FireRight");
            ProjectileSetup(Player, ChargedProjectilePrev);
            Cooldown = BaseCooldown;
        }

        void CooldownTick(float delta)
        {
            if (Cooldown > 0)
                Cooldown -= delta;
        }

        void ProjectileSetup(PlayerController player, PackedScene projectileScene)
        {
            var projectile = projectileScene.Instantiate<Area2D>();
            var projectileScript = projectile as PlayerProjectileMovement;
            var newGlobalX = player.GlobalPosition.X + ShotOriginOffset;
            var projectilePosition = new Vector2(newGlobalX, player.GlobalPosition.Y);
            projectile.GlobalPosition = projectilePosition;
            projectileScript.Player = player;
            player.Root.AddChild(projectile);
        }
    }
}