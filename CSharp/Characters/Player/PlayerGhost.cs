using System;
using Godot;

namespace ProtoVania
{
    public partial class PlayerGhost : Sprite2D
    {
        public override void _Ready()
        {
            var tween = GetTree().CreateTween();
            var color = new Color(1f, 1f, 1f, 0f);
            tween
                .TweenProperty(this, "modulate", color, 0.6f)
                .SetTrans(Tween.TransitionType.Sine)
                .SetEase(Tween.EaseType.Out);
            tween.TweenCallback(Callable.From(OnTweenCompleted));
        }

        public void OnTweenCompleted() => QueueFree();
    }
}