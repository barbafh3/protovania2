using Godot;

namespace ProtoVania
{
    public partial class PlayerHealth : Node2D, IDamageable
    {
        public PlayerController Player { get; private set; }

        [Export] private IntVariable _health = null;

        public IntVariable Health => _health;

        [Export] private float _iFramesDuration = 0f;

        public float IFramesDuration => _iFramesDuration;

        [Export] private float _baseIFramesCooldown = 0f;

        public float BaseIFramesCooldown => _baseIFramesCooldown;

        public float IFramesCooldown { get; set; }

        public override void _Ready()
        {
            Player = GetParent() as PlayerController;
            Utils.GetSignalManager(this).EmitSignal(SignalManager.SignalName.HealthChanged, Health.value);
        }

        public override void _PhysicsProcess(double delta)
        {
            IFramesCooldownTick((float)delta);
            if (Health.value <= 0)
                Die();
        }

        void IFramesCooldownTick(float delta)
        {
            if (IFramesCooldown > 0)
                IFramesCooldown -= delta;
        }

        public void TakeDamage(int amount)
        {
            if (IFramesCooldown <= 0f && Player.CanTakeDamage)
            {
                if (Health.value - amount <= 0)
                    Health.value = 0;
                else
                    Health.value -= amount;
                Player.audioPlayer.Stream = Player.Sounds.damage as AudioStream;
                Player.audioPlayer.Play();
                Utils.GetSignalManager(this).EmitSignal(SignalManager.SignalName.HealthChanged, Health.value);
                IFramesCooldown = BaseIFramesCooldown;
                GD.Print("Player health: " + Health.value);
            }
        }

        public void Die() => GD.Print("Player is dead");
    }
}