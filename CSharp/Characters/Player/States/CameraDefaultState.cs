using Godot;

namespace ProtoVania
{
    public partial class CameraDefaultState : Node2D, IState
    {
        public string StateName => "CameraDefault";

        CameraController Camera;

        public void EnterState(Node2D controller)
        {
            controller.GetNode<SignalManager>("/root/SignalManager").CameraShakeCalled += OnCameraShakeCall;
            Camera = controller as CameraController;
        }

        void OnCameraShakeCall(float power, float duration)
        {
            Camera.ShakePower = power;
            Camera.ShakeDuration = duration;
            Camera.ElapsedTime = 0;
            Camera.ChangeState(new CameraShakeState());
        }

        public void ExitState()
        {
        }
    }
}