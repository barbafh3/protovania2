using Godot;
using System;

namespace ProtoVania
{
    public partial class CameraShakeState : Node2D, IState, IProcessState
    {
        public string StateName
        {
            get { return "CameraShake"; }
        }

        CameraController Camera;

        public void EnterState(Node2D controller)
        {
            Camera = controller as CameraController;
            Camera.ElapsedTime = 0f;
        }

        public void Process(double delta)
        {
            if (Camera.ElapsedTime < Camera.ShakeDuration)
            {
                Camera.Offset = new Vector2((float)new Random().NextDouble(),
                    (float)new Random().NextDouble()
                ) * Camera.ShakePower;
                Camera.ElapsedTime += (float)delta;
            }
            else ExitState();
        }

        public void ExitState()
        {
            Camera.ElapsedTime = 0;
            Camera.Offset = Camera.CurrentPosition;
            Camera.ChangeState(new CameraDefaultState());
        }
    }
}