using Godot;
using System;

namespace ProtoVania
{
    public partial class PlayerClimbState : PlayerMobileState, IWallChecker, IState, IPhysicsProcessState
    {
        public String StateName => "PlayerClimb";

        public void EnterState(Node2D controller)
        {
            Player = controller as PlayerController;
            Player.JumpCount = Player.BaseJumpCount;
            Player.Velocity = new Vector2(0f, Player.Velocity.Y);
        }

        public void PhysicsProcess(double delta)
        {
            if ((this as IWallChecker).IsTouchingWalls(Player.LeftWallChecker, Player.RightWallChecker))
            {
                MoveVertical(Player, delta, lockX: true);
                ChangeFacing();
                PlayAnimation();
            }

            ExitState();
        }

        void ChangeFacing()
        {
            var fireScript = Utils.GetNode<PlayerFire>(Player);
            if (Player.LeftWallChecker.IsColliding())
            {
                Player.FaceDirection = 1;
                fireScript.ShotOriginOffset = Mathf.Abs(fireScript.ShotOriginOffset);
            }
            else if (Player.RightWallChecker.IsColliding())
            {
                Player.FaceDirection = -1;
                fireScript.ShotOriginOffset = -Mathf.Abs(fireScript.ShotOriginOffset);
            }
        }

        public void PlayAnimation(Node2D controller = null)
        {
            var vertical = Input.GetActionStrength("move_down") - Input.GetActionStrength("move_up");
            if (vertical != 0)
                Player.SetAnimation("ClimbMoveLeft", "ClimbMoveRight");
            else
                Player.SetAnimation("ClimbIdleLeft", "ClimbIdleRight");
        }

        public void ExitState()
        {
            IState state = null;
            if (Input.IsActionJustReleased("open_inventory"))
                state = new PlayerInventoryState();
            if (Player.GroundChecker.IsColliding() || Player.GroundChecker2.IsColliding())
            {
                var collider = Player.GroundChecker.GetCollider() as Node;
                if (collider != null && collider.IsInGroup("Terrain"))
                {
                    var horizontal = Input.GetActionStrength("move_right") - Input.GetActionStrength("move_left");
                    if (horizontal != 0)
                        state = new PlayerOnWallGroundState();
                }
            }

            if (Player.CanJump && Input.IsActionJustPressed("jump") && Player.JumpCount > 0)
                state = new PlayerWallJumpState();
            if (Player.CanDash && Input.IsActionJustPressed("dash") && Player.DashCooldown <= 0)
                state = new PlayerDashState();
            if (!(this as IWallChecker).IsTouchingWalls(Player.LeftWallChecker, Player.RightWallChecker))
                state = new PlayerFallState();
            else if (!Player.LeftLedgeChecker.IsColliding() && !Player.RightLedgeChecker.IsColliding())
                state = new PlayerLedgeGrabState();

            if (state != null)
                Player.ChangeState(state);
        }
    }
}