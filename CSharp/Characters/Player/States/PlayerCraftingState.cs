using Godot;
using System;

namespace ProtoVania
{
    public partial class PlayerCraftingState : PlayerMobileState, IState, IProcessState
    {
        public string StateName => "PlayerCraftingState";

        public void EnterState(Node2D controller)
        {
            Player = controller as PlayerController;
            GetNode<SignalManager>("/root/SignalManager").CraftingEnded += OnCraftingEnded;
        }

        public void Process(double delta)
        {
            MoveHorizontal(Player, (float)delta);
            if (Input.IsActionJustReleased("open_inventory"))
                Player.ChangeState(new PlayerInventoryState());
        }

        void OnCraftingEnded() => ExitState();

        public void ExitState() => Player.ChangeState(new PlayerIdleState());
    }
}