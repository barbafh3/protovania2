using Godot;
using System;

namespace ProtoVania
{
    public partial class PlayerDashState : Node2D, IState, IPhysicsProcessState
    {
        public String StateName => "PlayerDash";

        PlayerController Player;
        private float Angle;

        public void EnterState(Node2D controller)
        {
            Player = controller as PlayerController;
            Player.DashDuration = Player.BaseDashDuration;
            Angle = Player.GetAngleTo(Player.GetGlobalMousePosition());
            Player.GhostTimer.Timeout += GhostEffect;
        }

        public void PhysicsProcess(double delta)
        {
            DurationTick((float)delta);
            /* PlayAnimation(); */

            if (Player.DashDuration > 0f && Player.DashCooldown <= 0f)
            {
                Vector2 velocity = new Vector2();
                if (Player.MouseDirectionEnabled)
                    velocity = Player.DashSpeed * new Vector2(Mathf.Cos(Angle), Mathf.Sin(Angle));
                else
                {
                    if (Player.FaceDirection == 1)
                        velocity.X = Player.DashSpeed;
                    if (Player.FaceDirection == -1)
                        velocity.X = -Player.DashSpeed;
                }

                Player.MoveAndCollide(velocity);
                Player.audioPlayer.Stream = Player.Sounds.dash;
                Player.audioPlayer.Play();
            }
            else ExitState();
        }

        public void PlayAnimation(Node2D controller = null)
        {
            if (Player.FaceDirection == 1)
                Player.AnimatedSprite.Play("DashRight");
            if (Player.FaceDirection == -1)
                Player.AnimatedSprite.Play("DashLeft");
        }

        void GhostEffect() => Player.GenerateGhost();

        void DurationTick(float delta)
        {
            if (Player.DashDuration > 0f)
                Player.DashDuration -= delta;
        }

        public void ExitState()
        {
            if (Input.IsActionJustReleased("open_inventory"))
            {
                Player.ChangeState(new PlayerInventoryState());
                Player.DashCooldown = Player.BaseDashCooldown;
            }
            else
            {
                Player.ChangeState(new PlayerIdleState());
                Player.DashCooldown = Player.BaseDashCooldown;
            }
        }
    }
}