using Godot;
using System;

namespace ProtoVania
{
    public partial class PlayerFallState : PlayerMobileState, IWallChecker, IState, IPhysicsProcessState
    {
        public String StateName => "PlayerFall"; 

        public void EnterState(Node2D controller)
        {
            Player = controller as PlayerController;
            Player.Velocity = new Vector2(Player.Velocity.X, 0f);
        }

        public void PhysicsProcess(double delta)
        {
            MoveHorizontal(Player, (float)delta);
            Player.Flip();
            Player.SetAnimation("FallLeft", "FallRight");

            ExitState();
        }

        public void PlayAnimation(Node2D controller = null)
        {
            if (Player.FaceDirection == 1)
                Player.AnimatedSprite.Play("FallRight");
            else
                Player.AnimatedSprite.Play("FallLeft");
        }

        public void ExitState()
        {
            IState state = null;
            if (Input.IsActionJustReleased("open_inventory"))
                state = new PlayerInventoryState();
            if (Player.GroundChecker.IsColliding() || Player.GroundChecker2.IsColliding())
            {
                var collider = Player.GroundChecker.GetCollider() as Node;
                if (collider != null && collider.IsInGroup("Terrain"))
                {
                    Player.audioPlayer.Stream = Player.Sounds.land;
                    Player.audioPlayer.Play();
                    Player.SetAnimation("LandLeft", "LandRight");
                    state = new PlayerIdleState();
                }
            }

            if (Player.CanJump && Input.IsActionJustPressed("jump") && Player.JumpCount > 0)
                state = new PlayerJumpState();
            if (Player.CanDash && Input.IsActionJustPressed("dash") && Player.DashCooldown <= 0)
                state = new PlayerDashState();
            if (Player.CanClimb
                && (this as IWallChecker).IsTouchingWalls(Player.LeftWallChecker, Player.RightWallChecker))
                state = new PlayerOnWallAirState();

            if (state != null)
                Player.ChangeState(state);
        }
    }
}