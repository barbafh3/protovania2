using Godot;
using System;
using System.Collections.Generic;

namespace ProtoVania
{
    public partial class PlayerIdleState : PlayerMobileState, IWallChecker, IState, IProcessState, IPhysicsProcessState
    {
        public String StateName
        {
            get { return "PlayerIdle"; }
        }

        public void EnterState(Node2D character)
        {
            Player = character as PlayerController;
            Player.JumpCount = Player.BaseJumpCount;
            Player.DashDuration = Player.BaseDashDuration;
        }

        public void Process(double delta)
        {
            Player.Flip();
            Player.SetAnimation("IdleLeft", "IdleRight");

            Vector2 velocity = Player.Velocity;
            velocity.Y += Constants.Gravity * (float)delta;
            velocity.X = 0f;
            Player.Velocity = velocity;
            Player.MoveAndSlide();
        }

        public void PhysicsProcess(double delta) => ExitState();

        public void ExitState()
        {
            IState state = null;
            var horizontal = Input.GetActionStrength("move_right") - Input.GetActionStrength("move_left");

            if (Input.IsActionJustReleased("open_inventory"))
                state = new PlayerInventoryState();
            if (Player.GroundChecker.IsColliding() || Player.GroundChecker2.IsColliding())
            {
                var collider = Player.GroundChecker.GetCollider() as Node;
                if (collider == null || !collider.IsInGroup("Terrain"))
                {
                    if (Player.CanClimb
                        && (this as IWallChecker).IsTouchingWalls(Player.LeftWallChecker, Player.RightWallChecker))
                        state = new PlayerOnWallAirState();
                }
            }
            else
            {
                if (Player.Velocity.Y > 0f)
                    state = new PlayerFallState();
            }

            if (Player.CanRun && horizontal != 0)
                state = new PlayerRunState();
            if (Player.CanJump && Input.IsActionJustPressed("jump") && Player.JumpCount > 0)
                state = new PlayerJumpState();
            if (Player.CanDash && Input.IsActionJustPressed("dash") && Player.DashCooldown <= 0f)
                state = new PlayerDashState();
            if (Player.CanClimb
                && (this as IWallChecker).IsTouchingWalls(Player.LeftWallChecker, Player.RightWallChecker))
                state = new PlayerOnWallGroundState();

            if (state != null)
                Player.ChangeState(state);
        }
    }
}