using Godot;
using System;

namespace ProtoVania
{
    public partial class PlayerInventoryState : PlayerMobileState, IState, IProcessState, IPhysicsProcessState
    {
        public String StateName => "PlayerInventory";

        private SignalManager SignalManager;

        public void EnterState(Node2D controller)
        {
            Player = controller as PlayerController;
            SignalManager = Utils.GetSignalManager(this);
            SignalManager.CraftingStarted += ExitState;
            Player.Velocity = new Vector2(0f, Player.Velocity.Y);
            Player.InventoryUI.SetVisibility(true);
        }

        public void Process(double delta) => MoveHorizontal(Player, (float)delta);

        public void PhysicsProcess(double delta)
        {
            if (Input.IsActionJustReleased("open_inventory"))
                ExitState();
        }

        public void ExitState()
        {
            Player.InventoryUI.SetVisibility(false);
            SignalManager.EmitSignal(SignalManager.SignalName.InventoryClosed);
            Player.ChangeState(new PlayerIdleState());
        }
    }
}