using Godot;
using System;
using System.Collections.Generic;

namespace ProtoVania
{
    public partial class PlayerJumpState : PlayerMobileState, IWallChecker, IState, IPhysicsProcessState
    {
        public String StateName => "PlayerJump";

        public void EnterState(Node2D controller)
        {
            Player = controller as PlayerController;
            Player.JumpCount--;
            Player.audioPlayer.Stream = Player.Sounds.jump;
            Player.audioPlayer.Play();
            MoveHorizontal(Player, (float)Player.GetProcessDeltaTime(), Player.JumpForce);
        }

        public void PhysicsProcess(double delta)
        {
            MoveHorizontal(Player, (float)delta);
            Player.Flip();
            Player.SetAnimation("JumpLeft", "JumpRight");

            ExitState();
        }

        public void PlayAnimation()
        {
            if (Player.FaceDirection == 1)
                Player.AnimatedSprite.Play("JumpRight");
            else
                Player.AnimatedSprite.Play("JumpLeft");
        }

        public void ExitState()
        {
            IState state = null;
            if (Input.IsActionJustReleased("open_inventory"))
                state = new PlayerInventoryState();
            if (Player.GroundChecker.IsColliding() || Player.GroundChecker2.IsColliding())
            {
                var collider = Player.GroundChecker.GetCollider() as Node;
                if (collider != null && collider.IsInGroup("Terrain"))
                {
                    Player.audioPlayer.Stream = Player.Sounds.land;
                    Player.audioPlayer.Play();
                    state = new PlayerIdleState();
                }
            }

            if (Player.Velocity.Y > 0)
                state = new PlayerFallState();
            if ((this as IWallChecker).IsTouchingWalls(Player.LeftWallChecker, Player.RightWallChecker))
                state = new PlayerOnWallAirState();
            if (Player.CanJump && Input.IsActionJustPressed("jump") && Player.JumpCount > 0)
                state = new PlayerJumpState();
            if (Player.CanDash && Input.IsActionJustPressed("dash") && Player.DashCooldown <= 0)
                state = new PlayerDashState();

            if (state != null)
                Player.ChangeState(state);
        }
    }
}