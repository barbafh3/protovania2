using Godot;
using System;

namespace ProtoVania
{
    public partial class PlayerLedgeGrabState : Node2D, IWallChecker, IState, IProcessState
    {
        public String StateName => "PlayerLedgeGrab";

        PlayerController Player;
        TileMap Tilemap;
        Vector2 TilePosition;

        public void EnterState(Node2D controller)
        {
            Player = controller as PlayerController;
            Player.AnimatedSprite.AnimationFinished += AnimationFinished;
            bool hasCollision = CheckDestinationCollision();
            ChangePlayerFacing();
            GetTilePosition();
            if (hasCollision)
                Player.ChangeState(new PlayerClimbState());
            else
            {
                Player.GlobalPosition = Player.FaceDirection == 1 ?
                        new Vector2(TilePosition.X + 8, TilePosition.Y - 8) :
                        new Vector2(TilePosition.X - 8, TilePosition.Y - 8);

                PlayAnimation();
            }
        }

        public void Process(double delta)
        {
            if (!(this as IWallChecker).IsTouchingWalls(Player.LeftWallChecker, Player.RightWallChecker))
                Player.ChangeState(new PlayerFallState());
        }

        public void ChangePlayerFacing()
        {
            var fireScript = Player.GetNode<PlayerFire>("PlayerFire");
            if (Player.LeftWallChecker.IsColliding())
            {
                Player.FaceDirection = -1;
                fireScript.ShotOriginOffset = Mathf.Abs(fireScript.ShotOriginOffset);
            }

            if (Player.RightWallChecker.IsColliding())
            {
                Player.FaceDirection = 1;
                fireScript.ShotOriginOffset = -Mathf.Abs(fireScript.ShotOriginOffset);
            }
        }

        public void GetTilePosition()
        {
            var tileCoordinate = Player.Platforms.LocalToMap(Player.GlobalPosition);
            TilePosition = new Vector2((tileCoordinate.X * 32) + 16, (tileCoordinate.Y * 32) + 16);
        }

        public void PlayAnimation()
        {
            if (Player.LeftWallChecker.IsColliding())
            {
                var collider = Player.LeftWallChecker.GetCollider() as Node2D;
                if (collider != null && collider.IsInGroup("Terrain"))
                    Player.AnimatedSprite.Play("LedgeLeft");
            }
            else if (Player.RightWallChecker.IsColliding())
            {
                var collider = Player.RightWallChecker.GetCollider() as Node2D;
                if (collider != null && collider.IsInGroup("Terrain"))
                    Player.AnimatedSprite.Play("LedgeRight");
            }
        }

        void AnimationFinished() => ExitState();

        bool CheckDestinationCollision()
        {
            var hasCollided = true;
            if (Player.LeftWallChecker.IsColliding() && !Player.LeftLedgeChecker.IsColliding())
            {
                var collider = Player.LeftWallChecker.GetCollider() as Node2D;
                if (collider != null && collider.IsInGroup("Terrain"))
                    hasCollided = false;
            }
            else if (Player.RightWallChecker.IsColliding() && !Player.RightLedgeChecker.IsColliding())
            {
                var collider = Player.RightWallChecker.GetCollider() as Node2D;
                if (collider != null && collider.IsInGroup("Terrain"))
                    hasCollided = false;
            }

            if (hasCollided) return true;
            else return false;
        }

        public void ExitState()
        {
            IState state = null;
            Player.AnimatedSprite.AnimationFinished += AnimationFinished;
            if (Input.IsActionJustReleased("open_inventory"))
                state = new PlayerInventoryState();
            else if (Player.AnimatedSprite.Animation == "LedgeRight")
            {
                var newPosition = new Vector2(Player.GlobalPosition.X + 33, Player.GlobalPosition.Y - 22);
                Player.GlobalPosition = newPosition;
                state = new PlayerIdleState();
            }
            else if (Player.AnimatedSprite.Animation == "LedgeLeft")
            {
                var newPosition = new Vector2(Player.GlobalPosition.X - 33, Player.GlobalPosition.Y - 22);
                Player.GlobalPosition = newPosition;
                state = new PlayerIdleState();
            }

            if (state != null) Player.ChangeState(state);
        }
    }
}