using Godot;
using System;

namespace ProtoVania
{
    public partial class PlayerOnWallAirState : PlayerMobileState, IWallChecker, IState, IProcessState
    {
        public String StateName => "PlayerOnWallAir";

        private bool _freed;

        public void EnterState(Node2D controller) => Player = controller as PlayerController;

        public void Process(double delta)
        {
            ExitState();
            if (!_freed)
            {
                Player.Flip();
                PlayAnimation();
                
                if (Player.Velocity.Y < 0) MoveHorizontal(Player, (float)delta);
                else MoveHorizontal(Player, (float)delta, 0f, 0f, true);
            }
        }

        public void PlayAnimation(Node2D controller = null)
        {
            if (Player.FaceDirection == 1)
                Player.AnimatedSprite.Play("FallRight");
            else
                Player.AnimatedSprite.Play("FallLeft");
        }

        public void ExitState()
        {
            IState state = null;
            if (Input.IsActionJustReleased("open_inventory"))
                state = new PlayerInventoryState();
            if (!(this as IWallChecker).IsTouchingWalls(Player.RightWallChecker, Player.LeftWallChecker))
                state = new PlayerFallState();
            else if (!Player.LeftLedgeChecker.IsColliding() || !Player.RightLedgeChecker.IsColliding())
                state = new PlayerLedgeGrabState();
            if (Player.CanJump && Input.IsActionJustPressed("jump"))
            {
                if (Player.GroundChecker.IsColliding() || Player.GroundChecker2.IsColliding())
                {
                    var collider = Player.GroundChecker.GetCollider() as Node;
                    if (collider != null && collider.IsInGroup("Terrain"))
                        state = new PlayerOnWallGroundState();
                }
                else
                    state = new PlayerWallJumpState();
            }

            if (Player.CanClimb && (Input.IsActionJustPressed("move_down") || Input.IsActionJustPressed("move_up")))
                state = new PlayerClimbState();
            if (Player.GroundChecker.IsColliding() || Player.GroundChecker2.IsColliding())
            {
                var collider = Player.GroundChecker.GetCollider() as Node;
                if (collider != null && collider.IsInGroup("Terrain"))
                    state = new PlayerOnWallGroundState();
            }

            if (Player.CanDash && Input.IsActionJustPressed("dash") && Player.DashCooldown <= 0)
                state = new PlayerDashState();

            if (state != null)
            {
                _freed = true;
                CallDeferred("free");
                Player.ChangeState(state);
            }
        }
    }
}