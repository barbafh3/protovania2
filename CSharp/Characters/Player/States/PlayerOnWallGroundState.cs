using Godot;
using System;

namespace ProtoVania
{
    public partial class PlayerOnWallGroundState : PlayerMobileState, IWallChecker, IState, IProcessState
    {
        public String StateName => "PlayerOnWallGround";

        public void EnterState(Node2D controller) => Player = controller as PlayerController;

        public void Process(double delta)
        {
            MoveHorizontal(Player, (float)delta);
            Player.Flip();
            PlayAnimation();

            ExitState();
        }

        public void WallExited(Node2D body)
        {
            GD.Print("WallGround: Exited");
            if (body.IsInGroup("Terrain")) Player.ChangeState(new PlayerIdleState());
        }

        public void PlayAnimation()
        {
            if (Player.FaceDirection == 1)
                Player.AnimatedSprite.Play("IdleRight");
            else
                Player.AnimatedSprite.Play("IdleLeft");
        }

        public void ExitState()
        {
            IState state = null;
            if (!(this as IWallChecker).IsTouchingWalls(Player.LeftWallChecker, Player.RightWallChecker))
                state = new PlayerIdleState();
            if (Input.IsActionJustReleased("open_inventory"))
                state = new PlayerInventoryState();
            if (Player.Velocity.X != 0)
                state = new PlayerRunState();
            if (Player.CanJump && Input.IsActionJustPressed("jump"))
                state = new PlayerJumpState();
            if (Player.CanClimb && (Input.IsActionJustPressed("move_down") || Input.IsActionJustPressed("move_up")))
                state = new PlayerClimbState();

            if (state != null)
                Player.ChangeState(state);
        }
    }
}