using Godot;
using System;

namespace ProtoVania
{
    public partial class PlayerRunState : PlayerMobileState, IWallChecker, IState, IPhysicsProcessState
    {
        public String StateName => "PlayerRun";

        public void EnterState(Node2D controller) => Player = controller as PlayerController;

        public void PhysicsProcess(double delta)
        {
            MoveHorizontal(Player, (float)delta);
            Player.Flip();
            Player.SetAnimation("RunLeft", "RunRight");

            ExitState();
        }

        public void PlayAnimation(Node2D controller = null)
        {
            if (Player.FaceDirection == 1)
                Player.AnimatedSprite.Play("RunRight");
            else
                Player.AnimatedSprite.Play("RunLeft");
        }

        public void ExitState()
        {
            IState state = null;
            if (Input.IsActionJustReleased("open_inventory"))
                state = new PlayerInventoryState();
            if (Player.GroundChecker.IsColliding() || Player.GroundChecker2.IsColliding())
            {
                var collider = Player.GroundChecker.GetCollider() as Node;
                if (collider != null && collider.IsInGroup("Terrain"))
                {
                    var horizontal = Input.GetActionStrength("move_right") - Input.GetActionStrength("move_left");
                    if (horizontal == 0)
                        state = new PlayerIdleState();
                }
            }
            else
            {
                if (Player.Velocity.Y > 0f)
                    state = new PlayerFallState();
            }

            if (Player.CanJump && Input.IsActionJustPressed("jump") && Player.JumpCount > 0)
                state = new PlayerJumpState();
            if (Player.CanDash && Input.IsActionJustPressed("dash") && Player.DashCooldown <= 0)
                state = new PlayerDashState();
            if (Player.CanClimb
                && (this as IWallChecker).IsTouchingWalls(Player.LeftWallChecker, Player.RightWallChecker))
                state = new PlayerOnWallGroundState();

            if (state != null)
                Player.ChangeState(state);
        }
    }
}