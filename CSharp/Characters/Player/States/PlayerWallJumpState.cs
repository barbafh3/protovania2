using Godot;
using System;

namespace ProtoVania
{
    public partial class PlayerWallJumpState : PlayerMobileState, IWallChecker, IState, IPhysicsProcessState
    {
        public String StateName => "PlayerWallJump";

        float _newX = 0f;

        public void EnterState(Node2D controller)
        {
            Player = controller as PlayerController;
            Player.JumpCount--;
            Player.audioPlayer.Stream = Player.Sounds.jump;
            Player.audioPlayer.Play();
            if (Player.LeftWallChecker.IsColliding())
            {
                var collider = Player.LeftWallChecker.GetCollider() as Node;
                if (collider != null && collider.IsInGroup("Terrain"))
                    _newX = 1;
            }
            else if (Player.RightWallChecker.IsColliding())
            {
                var collider = Player.RightWallChecker.GetCollider() as Node;
                if (collider != null && collider.IsInGroup("Terrain"))
                    _newX = -1;
            }

            MoveHorizontal(Player, (float)Player.GetProcessDeltaTime(), Player.JumpForce, _newX);
        }

        public void PhysicsProcess(double delta)
        {
            MoveHorizontal(Player, (float)delta, 0f, _newX);
            Player.Flip();
            PlayAnimation();
            ExitState();
        }

        public void PlayAnimation(Node2D controller = null)
        {
            if (Player.FaceDirection == 1)
                Player.AnimatedSprite.Play("JumpRight");
            else
                Player.AnimatedSprite.Play("JumpLeft");
        }

        public void ExitState()
        {
            IState state = null;
            if (Input.IsActionJustReleased("open_inventory"))
                state = new PlayerInventoryState();
            if (Player.GroundChecker.IsColliding() || Player.GroundChecker2.IsColliding())
            {
                var collider = Player.GroundChecker.GetCollider() as Node;
                if (collider != null && collider.IsInGroup("Terrain"))
                {
                    Player.audioPlayer.Stream = Player.Sounds.land;
                    Player.audioPlayer.Play();
                    state = new PlayerIdleState();
                }
            }

            if (Player.Velocity.Y > 0)
                state = new PlayerFallState();
            if (Player.CanClimb
                && (this as IWallChecker).IsTouchingWalls(Player.LeftWallChecker, Player.RightWallChecker))
                state = new PlayerOnWallAirState();
            if (Player.CanJump && Input.IsActionJustPressed("jump") && Player.JumpCount > 0)
                state = new PlayerJumpState();
            if (Player.CanDash && Input.IsActionJustPressed("dash") && Player.DashCooldown <= 0)
                state = new PlayerDashState();

            if (state != null)
                Player.ChangeState(state);
        }
    }
}