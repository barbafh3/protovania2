using System;

namespace ProtoVania
{
    public static class Constants
    {
        public readonly static float Gravity = 2500f;
        public readonly static float ItemGravity = 5f;
        public readonly static int StepSize = 8;
        public readonly static Array LowerAngles = new float[3] { -135f, -90f, -45f };
        public readonly static Array UpperAngles = new float[3] { 135f, 90f, 45f };

    }
}
