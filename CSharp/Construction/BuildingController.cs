using Godot;
using System;

namespace ProtoVania
{
    public partial class BuildingController : Area2D, IStateMachine, IBuilding
    {
        public IState CurrentState { get; private set; } = null;

        [Export] public float DestroyBaseTime { get; set; } = 0f;
        [Export] public Inventory PlayerInventory;
        [Export] public BuildingData BuildingData;
        public float DestroyTime { get; set; } = 0f;
        public InventoryUI InventoryUi { get; set; }
        public AnimatedSprite2D AnimatedSprite { get; set; }
        public PlayerController Player { get; set; }
        public string Id { get; set; }

        public override void _Ready()
        {
            InitializeVariables();
            ChangeState(new BuildingFloatingState());
        }

        public override void _Process(double delta)
        {
            if (CurrentState == null)
                _Ready();
            if (CurrentState is IProcessState state)
                state.Process(delta);
        }

        public override void _PhysicsProcess(double delta)
        {
            if (CurrentState == null)
                _Ready();
            if (CurrentState is IPhysicsProcessState state)
                state.PhysicsProcess(delta);
        }

        public void InitializeVariables()
        {
            AnimatedSprite = GetNode<AnimatedSprite2D>("AnimatedSprite");
            DestroyTime = DestroyBaseTime;
        }

        public void ChangeState(IState state)
        {
            if (CurrentState is Node2D node2D) node2D.QueueFree();
            CurrentState = state;
            CurrentState.EnterState(this);
            GD.Print("BuildingController: State = " + CurrentState.StateName);
        }
    }
}
