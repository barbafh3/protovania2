using Godot;

namespace ProtoVania
{
    public partial class BuildingDropIn : Control
    {
        FurnaceController _furnace;

        public override void _Ready() => _furnace = GetParent() as FurnaceController;

        public bool CanDropData(Vector2 position, object data)
        {
            if (data is StackInfo) return true;
            return false;
        }

        public void DropData(Vector2 position, object data)
        {
            GD.Print("DropIn: dropped");
            var pair = _furnace.CheckRecipes(((StackInfo)data).InventoryItem.item.Id);
            bool isMaterialValid;
            if (pair.Value != null) isMaterialValid = true;
            else isMaterialValid = false;
            if (isMaterialValid)
            {
                GD.Print("DropIn: Material is valid");
                if (_furnace.inputSlot == null)
                {
                    var stackInfo = data as StackInfo;
                    var item = stackInfo.InventoryItem as InventoryItem;
                    _furnace.playerInventory.RemoveStack(stackInfo.InventoryIndex);
                    stackInfo.InventoryItem = null;
                    _furnace.inputSlot = item;
                    GD.Print("DropIn: Dropped " + _furnace.inputSlot.item.Name);
                    _furnace.playerInventory.EmitSignal(nameof(Inventory.ValuesChanged));
                    _furnace.EmitSignal(nameof(FurnaceController.FurnaceCraftStarted));
                }
                else
                {
                    var stackInfo = data as StackInfo;
                    if (stackInfo.InventoryItem.item.Id == _furnace.inputSlot.item.Id)
                    {
                        var total = _furnace.inputSlot.amount + stackInfo.InventoryItem.amount;
                        if (total <= _furnace.inputSlot.item.StackLimit)
                        {
                            _furnace.inputSlot.SetAmount(total);
                            _furnace.playerInventory.RemoveStack(stackInfo.InventoryIndex);
                            stackInfo.InventoryItem = null;
                            _furnace.playerInventory.EmitSignal(nameof(Inventory.ValuesChanged));
                            _furnace.EmitSignal(nameof(FurnaceController.FurnaceCraftStarted));
                        }
                        else
                        {
                            GD.Print("DropIn: Total " + total + " bigger than limit " +
                                     _furnace.inputSlot.item.StackLimit);
                            var overflow = total - _furnace.inputSlot.item.StackLimit;
                            _furnace.inputSlot.SetAmount(total - overflow);
                            stackInfo.InventoryItem.SetAmount(overflow);
                            _furnace.playerInventory.InsertStack(stackInfo.InventoryIndex, stackInfo.InventoryItem);
                            stackInfo.InventoryItem = null;
                            _furnace.playerInventory.EmitSignal(nameof(Inventory.ValuesChanged));
                            _furnace.EmitSignal(nameof(FurnaceController.FurnaceCraftStarted));
                        }
                    }
                    else
                    {
                        var item = stackInfo.InventoryItem as InventoryItem;
                        var currentItem = _furnace.inputSlot;
                        _furnace.playerInventory.InsertStack(stackInfo.InventoryIndex, currentItem);
                        stackInfo.InventoryItem = null;
                        _furnace.inputSlot = item;
                        GD.Print("DropIn: Dropped " + _furnace.inputSlot.item.Name);
                        _furnace.playerInventory.EmitSignal(nameof(Inventory.ValuesChanged));
                        _furnace.EmitSignal(nameof(FurnaceController.FurnaceCraftStarted));
                    }
                }
            }
        }
    }
}