using Godot;
using System;

namespace ProtoVania
{
    public partial class BuildingFloatingState : Node2D, IState, IProcessState, IPhysicsProcessState
    {
        public String StateName => "BuildingFloating";

        protected BuildingController Building;

        protected bool IsCollidingBody = false;
        protected bool IsCollidingArea = false;
        protected bool StartupEnded = false;

        float _newX;
        float _newY;
        int _collisionCount = 0;

        public void EnterState(Node2D controller)
        {
            Building = controller as BuildingController;
            ConnectSignals();
            if (Building != null)
            {
                Building.AnimatedSprite.SelfModulate = new Color(0, 1, 0, 1);
                Building.GlobalPosition = Building.GetGlobalMousePosition();
                _newX = RoundUp((int)Math.Truncate(Building.GlobalPosition.X), Constants.StepSize);
                _newY = RoundUp((int)Math.Truncate(Building.GlobalPosition.Y), Constants.StepSize);
            }
        }

        public void Process(double delta)
        {
            var mousePos = Building.GetGlobalMousePosition();

            if (mousePos.X >= Building.GlobalPosition.X + Constants.StepSize)
                _newX = Building.GlobalPosition.X + Constants.StepSize;
            else if (mousePos.X <= Building.GlobalPosition.X - Constants.StepSize)
                _newX = Building.GlobalPosition.X - Constants.StepSize;

            if (mousePos.Y >= Building.GlobalPosition.Y + Constants.StepSize)
                _newY = Building.GlobalPosition.Y + Constants.StepSize;
            else if (mousePos.Y <= Building.GlobalPosition.Y - Constants.StepSize)
                _newY = Building.GlobalPosition.Y - Constants.StepSize;

            _newX = RoundUp((int)(Math.Truncate(_newX)), Constants.StepSize);
            _newY = RoundUp((int)(Math.Truncate(_newY)), Constants.StepSize);

            var newPosition = new Vector2(_newX, _newY);
            Building.GlobalPosition = newPosition;
        }

        public void PhysicsProcess(double delta)
        {
            if (Input.IsActionJustReleased("cancel"))
            {
                SignalManager.Emit(this, SignalManager.SignalName.CraftingEnded);
                var item = new InventoryItem(((BuildingController)Building).BuildingData, 1);
                ((BuildingController)Building).PlayerInventory.AddItem(item);
                Building.Visible = false;
                Building.QueueFree();
                QueueFree();
            }
            else
            {
                ExitState();
                StartupEnded = true;
            }
        }


        int RoundUp(int numToRound, int multiple)
        {
            if (multiple == 0)
                return numToRound;

            int remainder = numToRound % multiple;
            if (remainder == 0)
                return numToRound;

            return numToRound + multiple - remainder;
        }

        void ConnectSignals()
        {
            Utils.GetSignalManager(this).NewCraftSelected += OnNewCraftSelected;
            Building.BodyEntered += OnBodyCollisionEnter;
            Building.BodyExited += OnBodyCollisionExit;
            Building.AreaEntered += OnAreaCollisionEnter;
            Building.AreaExited += OnAreaCollisionEnter;
        }

        void OnNewCraftSelected()
        {
            var item = new InventoryItem(((BuildingController)Building).BuildingData, 1);
            ((BuildingController)Building).PlayerInventory.AddItem(item);
            Building.QueueFree();
            QueueFree();
        }

        void OnAreaCollisionEnter(Node2D body)
        {
            _collisionCount += 1;
            ((IBuilding)Building).AnimatedSprite.SelfModulate = new Color(1, 0, 0, 1);
            IsCollidingArea = true;
        }

        void OnAreaCollisionExit(Node2D body)
        {
            IsCollidingArea = false;
            if (_collisionCount > 0) _collisionCount -= 1;
            if (_collisionCount <= 0)
                ((IBuilding)Building).AnimatedSprite.SelfModulate = new Color(0, 1, 0, 1);
        }

        void OnBodyCollisionEnter(Node2D body)
        {
            _collisionCount += 1;
            ((IBuilding)Building).AnimatedSprite.SelfModulate = new Color(1, 0, 0, 1);
            IsCollidingBody = true;
        }

        void OnBodyCollisionExit(Node2D body)
        {
            IsCollidingBody = false;
            if (_collisionCount > 0) _collisionCount -= 1;
            if (_collisionCount <= 0)
                ((IBuilding)Building).AnimatedSprite.SelfModulate = new Color(0, 1, 0, 1);
        }

        public virtual void ExitState()
        {
            if (Input.IsActionJustReleased("interact_ui") &&
                !((IBuilding)Building).InventoryUi.Visible && StartupEnded)
            {
                GD.Print("Body: " + IsCollidingBody + " - Area: " + IsCollidingArea);
                if (!IsCollidingBody && !IsCollidingArea)
                {
                    ((IBuilding)Building).AnimatedSprite.SelfModulate = new Color(1, 1, 1, 1);
                    ((IStateMachine)Building).ChangeState(new BuildingIdleState());
                }
            }
        }
    }
}