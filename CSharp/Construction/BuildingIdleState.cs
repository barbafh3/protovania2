using Godot;
using System;

namespace ProtoVania
{
    public partial class BuildingIdleState : Control, IState
    {
        public String StateName => "BuildingPlaced";

        BuildingController _building;

        bool _isMouseOver = false;

        public virtual void EnterState(Node2D controller)
        {
            _building = controller as BuildingController;
            if (_building != null)
            {
                _building.MouseEntered += OnMouseEnter;
                _building.MouseExited += OnMouseExit;
            }

            SignalManager.Emit(this, SignalManager.SignalName.CraftingEnded);
        }

        public virtual void Process(double delta) => DestroyBuilding(delta);

        public virtual void PhysicsProcess(float delta) =>
            ((IBuilding)_building).AnimatedSprite.SelfModulate = new Color(1, 1, 1, 1);

        public void DestroyBuilding(double delta)
        {
            if (((IBuilding)_building).DestroyTime > 0)
            {
                if (Input.IsActionPressed("cancel") && _isMouseOver) ((IBuilding)_building).DestroyTime -= (float)delta;
                else ((IBuilding)_building).DestroyTime = ((IBuilding)_building).DestroyBaseTime;
            }
            else
            {
                var item = new InventoryItem((_building).BuildingData, 1);
                (_building).PlayerInventory.AddItem(item);
                _building.QueueFree();
                QueueFree();
            }
        }

        void OnMouseEnter() => _isMouseOver = true;

        void OnMouseExit() => _isMouseOver = false;

        public virtual void ExitState() { }
    }
}