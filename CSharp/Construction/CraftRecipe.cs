using Godot;
using GC = Godot.Collections;
using SC = System.Collections.Generic;

namespace ProtoVania
{
  public class RecipeCost
  {
    public string id { get; private set; }
    public int amount { get; private set; }

    public RecipeCost() { }

    public RecipeCost(string id, int amount)
    {
      this.id = id;
      this.amount = amount;
    }
  }

  public class CraftRecipe
  {
    public string id { get; set; }
    public string craftedItemId { get; set; }
    public float craftingTime { get; set; }
    public SC.List<RecipeCost> materials { get; set; }

    public CraftRecipe() { }

    public CraftRecipe(string id, string craftedItemId, float craftingTime, SC.List<RecipeCost> materials)
    {
      this.id = id;
      this.craftedItemId = craftedItemId;
      this.craftingTime = craftingTime;
      this.materials = materials;
    }

  }
}