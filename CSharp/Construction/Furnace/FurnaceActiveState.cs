using Godot;

namespace ProtoVania
{
    public partial class FurnaceActiveState : Control, IState, IProcessState
    {
        public string StateName => "FurnaceActive";

        FurnaceController _furnace;
        CraftRecipeData _recipe;

        int _cost;
        int _count;

        public void EnterState(Node2D controller)
        {
            _furnace = controller as FurnaceController;
            var pair = _furnace.CheckRecipes(_furnace.inputSlot.item.Id);
            _recipe = pair.Value;
            _cost = pair.Key;
            _furnace.AnimatedSprite.Play("On");
            _count = _furnace.inputSlot.amount / _cost;
        }

        public void Process(double delta)
        {
            CraftDurationTick((float)delta);
            DestroyBuilding((float)delta);
            if (_count > 0)
            {
                if (_furnace.craftingTime <= 0)
                {
                    GD.Print("FurnaceActive: Crafting 1 " + _recipe.CraftedItem.Name);
                    _furnace.inputSlot.AddAmount(-_cost);
                    InventoryItem result = new InventoryItem(_recipe.CraftedItem, 1);
                    _furnace.ouputSlot = result;
                    var asset = ResourceLoader.Load(result.item.AssetPath) as PackedScene;
                    var instance = asset.Instantiate<Node2D>();
                    Vector2 newPosition = new Vector2(_furnace.GlobalPosition.X, _furnace.GlobalPosition.Y);
                    instance.GlobalPosition = newPosition;
                    var script = instance as PickableItem;
                    script.Amount = 1;
                    _furnace.GetTree().Root.AddChild(instance);
                    _count--;
                    GD.Print("FurnaceActive: Remaining materials = " + _furnace.inputSlot.amount +
                             " - Remaining crafts = " + _count);
                    _furnace.craftingTime = _recipe.CraftingTime;
                }
            }
            else ExitState();
        }

        void CraftDurationTick(float delta)
        {
            if (_furnace.craftingTime > 0) _furnace.craftingTime -= delta;
        }

        public void DestroyBuilding(float delta)
        {
            if (((IBuilding)_furnace).DestroyTime > 0)
            {
                if (Input.IsActionPressed("cancel") && _furnace.isMouseOver) ((IBuilding)_furnace).DestroyTime -= delta;
                else ((IBuilding)_furnace).DestroyTime = ((IBuilding)_furnace).DestroyBaseTime;
            }
            else
            {
                var item = new InventoryItem(_furnace.furnaceData, 1);
                var dropItem = _furnace.inputSlot;
                GD.Print("FurnaceActive: Item ID = " + dropItem.item.Id);
                GD.Print("FurnaceActive: AssetPath = " + dropItem.item.AssetPath);
                var asset = ResourceLoader.Load(dropItem.item.AssetPath) as PackedScene;
                var instance = asset.Instantiate<Node2D>();
                instance.GlobalPosition = _furnace.GlobalPosition;
                var script = instance as PickableItem;
                script.Amount = dropItem.amount;
                _furnace.GetTree().Root.AddChild(instance);
                _furnace.playerInventory.AddItem(item);
                _furnace.QueueFree();
                QueueFree();
            }
        }

        public void ExitState() => _furnace.ChangeState(new FurnaceIdleState());
    }
}