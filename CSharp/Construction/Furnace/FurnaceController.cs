using Godot;
using System.Collections.Generic;

namespace ProtoVania
{
    public partial class FurnaceController : Area2D, IStateMachine, IBuilding
    {
        [Signal]
        public delegate void FurnaceCraftStartedEventHandler();

        public IState CurrentState { get; private set; } = null;

        public InventoryUI InventoryUi { get; set; }
        public AnimatedSprite2D AnimatedSprite { get; set; }
        public PlayerController player { get; set; }
        public Control control { get; private set; }

        [Export] public float craftingTime = 0f;
        [Export] public float DestroyBaseTime { get; set; } = 0f;
        [Export] public Inventory playerInventory;
        [Export] public BuildingData furnaceData;
        [Export] public CraftRecipeData[] availableRecipes;
        public string Id { get; set; } = "";
        public float DestroyTime { get; set; } = 0f;

        public InventoryItem inputSlot = null;
        public InventoryItem ouputSlot = null;
        public bool isMouseOver = false;

        public override void _Ready()
        {
            InitializeVariables();
            ChangeState(new FurnaceFloatingState());
        }

        public override void _Process(double delta)
        {
            if (CurrentState == null) _Ready();
            if (CurrentState is IProcessState)
                ((IProcessState)CurrentState).Process(delta);
        }

        public override void _PhysicsProcess(double delta)
        {
            if (CurrentState == null) _Ready();
            if (CurrentState is IPhysicsProcessState)
                ((IPhysicsProcessState)CurrentState).PhysicsProcess(delta);
        }

        public void ChangeState(IState state)
        {
            if (CurrentState is Node2D) ((Node2D)CurrentState).QueueFree();
            CurrentState = state;
            CurrentState.EnterState(this);
            GD.Print("Furnace: State = " + CurrentState.StateName);
        }

        public void InitializeVariables()
        {
            DestroyTime = DestroyBaseTime;
            AnimatedSprite = GetNode<AnimatedSprite2D>("AnimatedSprite");
            control = GetNode<Control>("DropIn");
            if (control == null) GD.Print("FurnaceController: DropIn is null");
            control.MouseEntered += OnMouseEnter;
            control.MouseExited += OnMouseExit;
        }

        public KeyValuePair<int, CraftRecipeData> CheckRecipes(string materialId)
        {
            CraftRecipeData selectedRecipe = null;
            int cost = 0;
            foreach (var recipe in availableRecipes)
            {
                // var recipe = CraftRecipeData.GetRecipe(recipe);
                foreach (var material in recipe.Materials)
                {
                    if (material.Value.Id == materialId)
                    {
                        selectedRecipe = recipe;
                        cost = material.Key;
                    }
                }
            }

            return new KeyValuePair<int, CraftRecipeData>(cost, selectedRecipe);
        }

        void OnMouseEnter() => isMouseOver = true;

        void OnMouseExit() => isMouseOver = false;
    }
}