using Godot;

namespace ProtoVania
{
    public partial class FurnaceIdleState : Control, IState, IProcessState
    {
        public string StateName => "FurnaceIdle";

        FurnaceController Furnace;

        public void EnterState(Node2D controller)
        {
            Furnace = controller as FurnaceController;
            Furnace.AnimatedSprite.Play("Off");
            Furnace.FurnaceCraftStarted += ExitState;
            Utils.GetSignalManager(this).EmitSignal(SignalManager.SignalName.CraftingEnded);
        }

        public void Process(double delta)
        {
            DestroyBuilding((float)delta);
            if (Furnace.inputSlot?.amount <= 0) Furnace.inputSlot = null;
        }

        public void DestroyBuilding(float delta)
        {
            if (((IBuilding)Furnace).DestroyTime > 0)
            {
                if (Input.IsActionPressed("cancel") && Furnace.isMouseOver) ((IBuilding)Furnace).DestroyTime -= delta;
                else ((IBuilding)Furnace).DestroyTime = ((IBuilding)Furnace).DestroyBaseTime;
            }
            else
            {
                var item = new InventoryItem(Furnace.furnaceData, 1);
                if (Furnace.inputSlot != null)
                {
                    var dropItem = Furnace.inputSlot;
                    var asset = ResourceLoader.Load(dropItem.item.AssetPath) as PackedScene;
                    var instance = asset.Instantiate<Node2D>();
                    instance.GlobalPosition = Furnace.GlobalPosition;
                    var script = instance as PickableItem;
                    script.Amount = dropItem.amount;
                    Furnace.GetTree().Root.AddChild(instance);
                }

                Furnace.playerInventory.AddItem(item);
                Furnace.QueueFree();
                QueueFree();
            }
        }

        public void ExitState() => Furnace.ChangeState(new FurnaceActiveState());
    }
}