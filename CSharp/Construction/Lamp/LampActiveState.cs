using Godot;
using GC = Godot.Collections;

namespace ProtoVania
{
    public partial class LampActiveState : Node2D, IState, IProcessState
    {
        public string StateName => "LampActive";

        LampController Lamp;

        public void EnterState(Node2D controller)
        {
            Lamp = controller as LampController;
            Lamp.AnimatedSprite.Play("On");
            Lamp.Lights.Visible = true;
        }

        public void Process(double delta)
        {
            Lamp.DestroyBuilding(this, (float)delta);
            if (!Lamp.Powered)
                ExitState();
        }

        public void ExitState() => Lamp.ChangeState(new LampIdleState());
    }
}