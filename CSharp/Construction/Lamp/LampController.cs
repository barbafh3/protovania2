using Godot;

namespace ProtoVania
{
    public partial class LampController : Area2D, IStateMachine, IBuilding, IPowerSpender
    {
        public IState CurrentState { get; private set; } = null;

        [Export] public float DestroyBaseTime { get; set; } = 0f;
        [Export] public int PowerDrainage { get; set; } = 0;
        [Export] public Inventory PlayerInventory;
        [Export] public BuildingData LampData;

        public bool Powered { get; set; } = false;
        public string Id { get; set; } = "";
        public AnimatedSprite2D AnimatedSprite { get; set; }
        public InventoryUI InventoryUi { get; set; }
        public PlayerController Player { get; set; }
        public Control Control { get; set; }
        public Node2D Lights = null;
        public float DestroyTime { get; set; } = 0f;
        public bool IsMouseOver = false;

        float _deltaRef;

        public override void _Ready()
        {
            InitializeVariables();
            ChangeState(new LampFloatingState());
        }

        public override void _Process(double delta)
        {
            if (CurrentState == null)
                ChangeState(new LampFloatingState());
            if (CurrentState is IProcessState state)
                state.Process(delta);
        }

        public override void _PhysicsProcess(double delta)
        {
            if (CurrentState == null)
                ChangeState(new LampFloatingState());
            if (CurrentState is IPhysicsProcessState state)
                state.PhysicsProcess(delta);
        }

        public void InitializeVariables()
        {
            AnimatedSprite = GetNode<AnimatedSprite2D>("AnimatedSprite");
            Control = GetNode<Control>("TextureRect");
            Lights = GetNode<Node2D>("Lights");
            Control.MouseEntered += OnMouseEnter;
            Control.MouseExited += OnMouseExit;
            DestroyTime = DestroyBaseTime;
        }

        public void ChangeState(IState state)
        {
            if (CurrentState != null) (CurrentState as Node2D)?.QueueFree();
            CurrentState = state;
            CurrentState.EnterState(this);
            GD.Print("LampController: State = " + CurrentState.StateName);
        }

        void OnMouseEnter() => IsMouseOver = true;

        void OnMouseExit() => IsMouseOver = false;

        public void DestroyBuilding(IState state, float delta)
        {
            if (DestroyTime > 0)
            {
                if (Input.IsActionPressed("cancel") && IsMouseOver)
                    DestroyTime -= delta;
                else
                    DestroyTime = DestroyBaseTime;
            }
            else
            {
                var item = new InventoryItem(LampData, 1);
                PlayerInventory.AddItem(item);
                PowerGrid.Unregister("PowerSpender", this);
                if (state is Node2D) ((Node2D)state).QueueFree();
                QueueFree();
            }
        }
    }
}