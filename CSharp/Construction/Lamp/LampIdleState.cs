using Godot;

namespace ProtoVania
{
    public partial class LampIdleState : Node2D, IState, IProcessState
    {
        public string StateName => "LampIdle";

        LampController _lamp;
        Sprite2D _powerIndicator;
        float _blinkBaseTimer = 1f;
        float _blinkTimer;

        public void EnterState(Node2D controller)
        {
            _lamp = controller as LampController;
            if (_lamp != null)
            {
                _lamp.AnimatedSprite.Play("Off");
                _lamp.Lights.Visible = false;
                _blinkTimer = _blinkBaseTimer;
                _powerIndicator = _lamp.GetNode<Sprite2D>("PowerIndicator");
                _powerIndicator.Visible = true;
                Utils.GetSignalManager(this).EmitSignal(SignalManager.SignalName.CraftingEnded);
                PowerGrid.Register("PowerSpender", _lamp);
            }
        }

        public void Process(double delta)
        {
            BlinkPowerIndicator((float)delta);
            _lamp.DestroyBuilding(this, (float)delta);
            if (_lamp.Powered)
                ExitState();
        }

        void BlinkPowerIndicator(float delta)
        {
            if (_blinkTimer > 0)
                _blinkTimer -= delta;
            else
            {
                _powerIndicator.Visible = !_powerIndicator.Visible;
                _blinkTimer = _blinkBaseTimer;
            }
        }

        public void ExitState()
        {
            _blinkTimer = _blinkBaseTimer;
            _powerIndicator.Visible = false;
            _lamp.ChangeState(new LampActiveState());
        }
    }
}