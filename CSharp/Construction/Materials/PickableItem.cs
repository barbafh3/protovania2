using Godot;

namespace ProtoVania
{
    public partial class PickableItem : Area2D, IPickable
    {
        [Export] public int Amount = 0;
        [Export] public Inventory PlayerInventory;
        [Export] public ItemData ItemResource;
        public Item Item;
        public InventoryItem InventoryItem;

        Vector2 _velocity = new Vector2();

        bool _isOnGround = false;

        public override void _Ready()
        {
            InitializeVariables();
            GetNode<Area2D>("Area2D").BodyEntered += GiveMaterial;
        }

        public override void _PhysicsProcess(double delta)
        {
            if (!_isOnGround)
                GlobalPosition = new Vector2(GlobalPosition.X, GlobalPosition.Y + Constants.ItemGravity);
        }

        void InitializeVariables()
        {
            InventoryItem = new InventoryItem(ItemResource, Amount);
        }

        public void GiveMaterial(Node2D body)
        {
            if (body.IsInGroup("Player"))
            {
                GD.Print("Give Material: " + InventoryItem.amount);
                var operation = PlayerInventory.AddItem(InventoryItem);
                if (operation.added)
                {
                    if (operation.overflow <= 0) QueueFree();
                    else
                    {
                        InventoryItem.SetAmount(operation.overflow);
                        GiveMaterial(body);
                    }
                }
                else GD.Print("Failed to add item to inventory");
            }

            if (body.IsInGroup("Terrain"))
                _isOnGround = true;
        }
    }
}