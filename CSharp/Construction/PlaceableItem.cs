using Godot;

namespace ProtoVania
{
    public partial class PlaceableItem : Item, IUsable
    {
        public PlaceableItem() { }

        public PlaceableItem(
            string id,
            string name,
            Texture iconPath,
            int stackLimit,
            string assetPath,
            int baseHealth
        ) : base(id, name, iconPath, stackLimit, assetPath, baseHealth)
        {
        }

        public void Use(InventoryUI ui, Viewport root, PlayerController player) => SpawnBuilding(ui, root);

        public void SpawnBuilding(InventoryUI ui, Viewport root)
        {
            var assetScene = ResourceLoader.Load(this.AssetPath) as PackedScene;
            var instance = assetScene.Instantiate();
            var script = instance as IBuilding;
            script.InventoryUi = ui;
            script.Id = this.Id;
            root.AddChild(instance);
            instance.GetNode<SignalManager>("/root/SignalManager").EmitSignal("CraftingStarted");
        }
    }
}