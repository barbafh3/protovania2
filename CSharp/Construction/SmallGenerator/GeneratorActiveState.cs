using Godot;

namespace ProtoVania
{
    public partial class GeneratorActiveState : Node2D, IState, IProcessState
    {
        public string StateName => "GeneratorActive";

        GeneratorController _generator;

        public void EnterState(Node2D controller) => _generator = controller as GeneratorController;

        public void Process(double delta) => _generator.DestroyBuilding(this, (float)delta);

        public void ExitState() => _generator.ChangeState(new GeneratorIdleState());
    }
}