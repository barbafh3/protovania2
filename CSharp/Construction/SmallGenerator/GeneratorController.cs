using Godot;

namespace ProtoVania
{
    public partial class GeneratorController : Area2D, IStateMachine, IBuilding, IPowerSource
    {
        public IState CurrentState { get; private set; } = null;

        [Export] public Inventory PlayerInventory;
        [Export] public float DestroyBaseTime { get; set; } = 0f;
        [Export] public int powerCapacity { get; set; } = 0;
        [Export] public BuildingData GeneratorData;
        public string Id { get; set; } = "";
        public AnimatedSprite2D AnimatedSprite { get; set; }
        public Control Control { get; set; }
        public InventoryUI InventoryUi { get; set; }
        public float DestroyTime { get; set; } = 0f;
        public CircleShape2D PowerArea;
        public bool IsMouseOver = false;

        float _deltaRef;

        public override void _Ready()
        {
            InitializeVariables();
            ConnectSignals();
            ChangeState(new GeneratorFloatingState());
        }

        public override void _Process(double delta)
        {
            if (CurrentState == null)
                ChangeState(new GeneratorFloatingState());
            if (CurrentState is IProcessState state)
                state.Process(delta);
        }

        public override void _PhysicsProcess(double delta)
        {
            if (CurrentState == null)
                ChangeState(new GeneratorFloatingState());
            if (CurrentState is IPhysicsProcessState state)
                state.PhysicsProcess(delta);
        }

        public void InitializeVariables()
        {
            var powerSource = GetNode("PowerSource/CollisionShape2D") as CollisionShape2D;
            PowerArea = powerSource?.Shape as CircleShape2D;
            AnimatedSprite = GetNode<AnimatedSprite2D>("AnimatedSprite");
            Control = GetNode<Control>("TextureRect");
            DestroyTime = DestroyBaseTime;
        }

        public void ConnectSignals()
        {
            Control.MouseEntered += OnMouseEnter;
            Control.MouseExited += OnMouseExit;
        }

        public void ChangeState(IState state)
        {
            if (CurrentState != null) (CurrentState as Node2D)?.QueueFree();
            CurrentState = state;
            CurrentState.EnterState(this);
            GD.Print("Generator: State = " + CurrentState.StateName);
        }

        void OnMouseEnter() => IsMouseOver = true;

        void OnMouseExit() => IsMouseOver = false;

        public void DestroyBuilding(IState state, float delta)
        {
            if (DestroyTime > 0)
            {
                if (Input.IsActionPressed("cancel") && IsMouseOver)
                    DestroyTime -= delta;
                else
                    DestroyTime = DestroyBaseTime;
            }
            else
            {
                var item = new InventoryItem(GeneratorData, 1);
                PlayerInventory.AddItem(item);
                PowerGrid.Unregister("PowerSource", this);
                if (state is Node2D) ((Node2D)state).QueueFree();
                QueueFree();
            }
        }
    }
}