using Godot;
using System;

namespace ProtoVania
{
  public partial class GeneratorFloatingState : BuildingFloatingState
  {
    public override void ExitState()
    {
      if (Input.IsActionJustReleased("interact_ui") &&
          !((IBuilding)Building).InventoryUi.Visible && StartupEnded)
      {
        GD.Print("Body: " + IsCollidingBody + " - Area: " + IsCollidingArea);
        if (!IsCollidingBody && !IsCollidingArea)
        {
          ((IBuilding)Building).AnimatedSprite.SelfModulate = new Color(1, 1, 1, 1);
          ((IStateMachine)Building).ChangeState(new GeneratorIdleState());
        }
      }
    }
  }
}