using Godot;

namespace ProtoVania
{
    public partial class GeneratorIdleState : Node2D, IState, IProcessState
    {
        public string StateName => "GeneratorIdle";

        GeneratorController _generator;

        public void EnterState(Node2D controller)
        {
            _generator = controller as GeneratorController;
            SignalManager.Emit(this, SignalManager.SignalName.CraftingEnded);
            PowerGrid.Register("PowerSource", _generator);
        }

        public void Process(double delta) => _generator.DestroyBuilding(this, (float)delta);

        public void ExitState() => _generator.ChangeState(new GeneratorActiveState());
    }
}