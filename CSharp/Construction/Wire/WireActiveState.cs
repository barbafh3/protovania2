using System.Collections.Generic;
using Godot;

namespace ProtoVania.Wire;

public partial class WireActiveState : Node2D, IState, IProcessState
{
    public string StateName => "WireActiveState";
    private WireController Wire;
    
    public void EnterState(Node2D controller) => Wire = controller as WireController;

    public void Process(double delta)
    {
        Wire.AnimatedSprite.Play(SelectAnimation(CheckNearbyWireCollisions()));
    }

    public void ExitState()
    {
        // Not needed so far
    }

    Dictionary<string, bool> CheckNearbyWireCollisions()
    {
        var HasRightWire = RaycastUtils.CheckCompositeCollision(
            new List<RayCast2D>()
            {
                (RayCast2D)Wire.RightRaycasts.GetChild(0),
                (RayCast2D)Wire.RightRaycasts.GetChild(1),
                (RayCast2D)Wire.RightRaycasts.GetChild(2),
            }, "Wire", Wire);
        var HasLeftWire = RaycastUtils.CheckCompositeCollision(
            new List<RayCast2D>()
            {
                (RayCast2D)Wire.LeftRaycasts.GetChild(0),
                (RayCast2D)Wire.LeftRaycasts.GetChild(1),
                (RayCast2D)Wire.LeftRaycasts.GetChild(2),
            }, "Wire", Wire);
        var HasUpWire = RaycastUtils.CheckCompositeCollision(
            new List<RayCast2D>()
            {
                (RayCast2D)Wire.UpRaycasts.GetChild(0),
                (RayCast2D)Wire.UpRaycasts.GetChild(1),
                (RayCast2D)Wire.UpRaycasts.GetChild(2),
            }, "Wire", Wire);
        var HasDownWire = RaycastUtils.CheckCompositeCollision(
            new List<RayCast2D>()
            {
                (RayCast2D)Wire.DownRaycasts.GetChild(0),
                (RayCast2D)Wire.DownRaycasts.GetChild(1),
                (RayCast2D)Wire.DownRaycasts.GetChild(2),
            }, "Wire", Wire);
        return new()
        {
            { "Right", HasRightWire },
            { "Left", HasLeftWire },
            { "Up", HasUpWire },
            { "Down", HasDownWire }
        };
    }

    string SelectAnimation(Dictionary<string, bool> collisions)
    {
        var animation = "";

        if (collisions["Left"])
            animation += "Left";
        if (collisions["Up"])
            animation += "Up";
        if (collisions["Down"])
            animation += "Down";
        if (collisions["Right"])
            animation += "Right";

        if (animation == "" || animation == "Right" || animation == "Left")
            return "LeftRight";
        else if (animation == "Up" || animation == "Up")
            return "UpDown";

        return animation;
    }
}