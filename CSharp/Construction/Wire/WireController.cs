using Godot;

namespace ProtoVania.Wire;

public partial class WireController : Area2D, IStateMachine
{
    public IState CurrentState { get; private set; }
    
    [Export] public float DestroyBaseTime;

    private InventoryUI InventoryUi;

    public AnimatedSprite2D AnimatedSprite { get; private set; }
    public Node2D LeftRaycasts { get; private set; }
    public Node2D RightRaycasts { get; private set; }
    public Node2D UpRaycasts { get; private set; }
    public Node2D DownRaycasts { get; private set; }

    private double _deltaRef;

    public override void _Ready()
    {
        InitializeVariables();
        ChangeState(new WireFloatingState());
    }

    public override void _Process(double delta)
    {
        _deltaRef = delta;
        if (CurrentState == null)
            _Ready();
        if (CurrentState is IProcessState state)
            state.Process(delta);
    }

    public override void _PhysicsProcess(double delta)
    {
        if (CurrentState == null)
            _Ready();
        if (CurrentState is IPhysicsProcessState state)
            state.PhysicsProcess(delta);
    }

    public void InitializeVariables()
    {
        AnimatedSprite = GetNode<AnimatedSprite2D>("AnimatedSprite");
        LeftRaycasts = GetNode<Node2D>("Left");
        RightRaycasts = GetNode<Node2D>("Right");
        UpRaycasts = GetNode<Node2D>("Up");
        DownRaycasts = GetNode<Node2D>("Down");
    }

    public void ChangeState(IState state)
    {
        (CurrentState as Node)?.QueueFree();
        CurrentState = state;
        CurrentState.EnterState(this);
    }
}