using Godot;

namespace ProtoVania.Wire;

public partial class WireFloatingState : BuildingFloatingState
{
    public override void ExitState()
    {
        if (Input.IsActionJustReleased("interact_ui")
            && !Building.InventoryUi.Visible
            && StartupEnded
            && !IsCollidingArea && !IsCollidingBody)
        {
            Building.AnimatedSprite.SelfModulate = new Color(1, 1, 1, 1);
            Building.ChangeState(new WireActiveState());
        }
            
    }
}