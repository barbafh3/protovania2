using Godot;
using System.Collections.Generic;

namespace ProtoVania
{
  public partial class CraftRecipeData : Resource
  {

    private static List<CraftRecipe> _recipes = new List<CraftRecipe>();

    public static CraftRecipe GetRecipe(string id)
    {
      CraftRecipe namedItem = null;
      foreach (var item in _recipes)
        if (item.id == id) namedItem = item;
      return namedItem;
    }

    public CraftRecipeData()
    {
      // Iron Bar
      _recipes.Add(new CraftRecipe(
          "CR00001",
          "CM00001",
          2,
          new List<RecipeCost>() {
              new RecipeCost("RM00002", 5),
          }
      ));
      // Copper Bar
      _recipes.Add(new CraftRecipe(
          "CR00002",
          "CM00002",
          2,
          new List<RecipeCost>() {
              new RecipeCost("RM00003", 5),
          }
      ));
      // Small Generator
      _recipes.Add(new CraftRecipe(
          "CR00003",
          "BD00001",
          5,
          new List<RecipeCost>() {
              new RecipeCost("CM00001", 10),
              new RecipeCost("CM00002", 5),
          }
      ));
      // Lamp
      _recipes.Add(new CraftRecipe(
          "CR00004",
          "BD00002",
          3,
          new List<RecipeCost>() {
              new RecipeCost("CM00001", 5),
              new RecipeCost("CM00002", 1),
          }
      ));
      // Furnace
      _recipes.Add(new CraftRecipe(
          "CR00005",
          "BD00003",
          5,
          new List<RecipeCost>() {
              new RecipeCost("RM00001",10)
          }
      ));
    }
  }
}