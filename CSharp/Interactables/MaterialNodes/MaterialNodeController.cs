using Godot;
using System.Collections.Generic;

namespace ProtoVania
{
    public partial class MaterialNodeController : Area2D, IStateMachine
    {
        public IState CurrentState { get; private set; } = null;

        public List<string> PossibleOres { get; private set; } = new List<string>();

        [Export] private int _baseMaterialAmount = 0;

        public int BaseMaterialAmount => _baseMaterialAmount;

        [Export] private float _baseExtractionTimer = 0f;

        public float BaseExtractionTimer => _baseExtractionTimer;

        [Export] private string _materialId = "";

        public string MaterialId => _materialId;

        [Export] private int _extractionAmount = 10;

        public int ExtractionAmount => _extractionAmount;

        [Export] private Inventory _playerInventory = null;

        public Inventory PlayerInventory => _playerInventory;

        [Export] private ItemData _itemData = null;

        public ItemData ItemData => _itemData;

        public PackedScene FloatingTextScene => ResourceLoader.Load("res://Scenes/UI/FloatingText.tscn") as PackedScene;

        public AnimatedSprite2D Indicator { get; private set; }
        public AnimatedSprite2D Body { get; private set; }
        public PlayerController Player { get; private set; }
        public ProgressBar ProgressBar { get; private set; }
        public int MaterialAmount { get; set; } = 0;
        public float ExtractionTimer { get; set; } = 0f;
        public bool IsPlayerPresent { get; set; } = false;

        public override void _Ready()
        {
            InitializeVariables();
            ConnectSignals();
            ChangeState(new NodeIdleState());
        }

        public override void _Process(double delta)
        {
            if (CurrentState == null)
                ChangeState(new NodeIdleState());
            if (CurrentState is IProcessState state)
                state.Process(delta);
        }

        public override void _PhysicsProcess(double delta)
        {
            if (CurrentState == null)
                ChangeState(new NodeIdleState());
            if (CurrentState is IPhysicsProcessState state)
                state.PhysicsProcess(delta);
        }


        void InitializeVariables()
        {
            Indicator = GetNode<AnimatedSprite2D>("Indicator");
            Body = GetNode<AnimatedSprite2D>("Sprite");
            ProgressBar = GetNode<ProgressBar>("ProgressBar");
            MaterialAmount = BaseMaterialAmount;
            Indicator.Visible = false;
            ExtractionTimer = BaseExtractionTimer;
        }

        public void ChangeState(IState state)
        {
            if (CurrentState != null) (CurrentState as Node2D)?.QueueFree();
            CurrentState = state;
            CurrentState.EnterState(this);
        }

        void ConnectSignals()
        {
            BodyEntered += OnBodyEntered;
            BodyExited += OnBodyExited;
            AreaEntered += OnBodyEntered;
            AreaExited += OnBodyExited;
            GD.Print("Node: Signals connected");
        }

        void OnBodyEntered(Node2D body)
        {
            if (body.IsInGroup("Player"))
            {
                IsPlayerPresent = true;
                Player = body as PlayerController;
            }
        }

        void OnBodyExited(Node2D body)
        {
            if (body.IsInGroup("Player")) IsPlayerPresent = false;
        }
    }
}