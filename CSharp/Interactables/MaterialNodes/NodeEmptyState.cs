using Godot;

namespace ProtoVania
{
    public partial class NodeEmptyState : Node2D, IState, IProcessState
    {
        public string StateName => "NodeEmpty";

        MaterialNodeController Node;

        public void EnterState(Node2D controller)
        {
            Node = controller as MaterialNodeController;
            if (Node != null)
            {
                Node.Indicator.Visible = true;
                Node.Indicator.Play("empty");
                Node.Body.Play("empty");
            }

            Utils.GetSignalManager(this).ResetNodes += RefreshNode;
        }

        public void Process(double delta)
        {
            Node.Indicator.Visible = Node.IsPlayerPresent;
        }

        void RefreshNode()
        {
            Node.MaterialAmount = Node.BaseMaterialAmount;
            ExitState();
        }

        public void ExitState() => Node.ChangeState(new NodeIdleState());
    }
}