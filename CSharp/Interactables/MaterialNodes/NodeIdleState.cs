using Godot;

namespace ProtoVania
{
    public partial class NodeIdleState : Node2D, IState, IProcessState, IPhysicsProcessState
    {
        public string StateName => "NodeIdle";

        public MaterialNodeController Node;

        bool _isBeingMined = false;

        public void EnterState(Node2D controller)
        {
            Node = controller as MaterialNodeController;
            if (Node != null)
            {
                Node.Indicator.Play("default");
                Node.Body.Play("default");
                Node.ProgressBar.Visible = false;
            }
        }

        public void Process(double delta)
        {
            if (Node.IsPlayerPresent && !_isBeingMined) Node.Indicator.Visible = true;
            else Node.Indicator.Visible = false;
        }

        public void PhysicsProcess(double delta)
        {
            if (Input.IsActionPressed("interact_world") && Node.IsPlayerPresent)
            {
                ExtractionTick((float)delta);
                if (Node.MaterialAmount > 0)
                {
                    if (Node.ExtractionTimer <= 0)
                    {
                        Node.MaterialAmount -= Node.ExtractionAmount;
                        Node.ExtractionTimer = Node.BaseExtractionTimer;
                        var floatingText = Node.FloatingTextScene.Instantiate<FloatingText>();
                        floatingText.GlobalPosition = new Vector2(Node.GlobalPosition.X, Node.GlobalPosition.Y - 10);
                        floatingText.ZIndex = 100;
                        floatingText.SetText("+" + 10);
                        floatingText.SetIcon(Node.ItemData.Icon);
                        Node.GetTree().Root.AddChild(floatingText);
                        Node.PlayerInventory.AddItem(new InventoryItem(Node.ItemData, Node.ExtractionAmount));
                        GD.Print("Node: amount left = " + Node.MaterialAmount);
                    }
                    else
                    {
                        _isBeingMined = true;
                        Node.ProgressBar.Visible = true;
                    }
                }
                else
                {
                    Node.ProgressBar.Visible = false;
                    ExitState();
                }
            }
            else
            {
                _isBeingMined = false;
                Node.ProgressBar.Visible = false;
            }
        }

        void ExtractionTick(float delta)
        {
            if (Node.ExtractionTimer > 0)
            {
                Node.ExtractionTimer -= delta;
                Node.ProgressBar.Value = Node.ExtractionTimer;
            }
        }

        public void ExitState() => Node.ChangeState(new NodeEmptyState());
    }
}