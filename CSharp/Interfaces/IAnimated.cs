using Godot;
using System;

namespace ProtoVania
{
  public interface IAnimated
  {
    void PlayAnimation(Node2D controller);
    void Flip(Node2D controller);
  }
}