using Godot;
using System;

namespace ProtoVania
{
  public interface IBuilding
  {
    string Id { get; set; }
    AnimatedSprite2D AnimatedSprite { get; set; }
    InventoryUI InventoryUi { get; set; }
    float DestroyBaseTime { get; set; }
    float DestroyTime { get; set; }

    void InitializeVariables();
  }
}