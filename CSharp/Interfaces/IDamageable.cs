namespace ProtoVania
{
  public interface IDamageable
  {
    void TakeDamage(int amount);
    void Die();
  }
}
