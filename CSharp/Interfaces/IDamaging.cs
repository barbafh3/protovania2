using Godot;

namespace ProtoVania
{
    public interface IDamaging
    {
        int damage { get; set; }

        void DoDamage(Node2D body);
    }
}
