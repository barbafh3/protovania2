using Godot;
using System.Collections.Generic;

namespace ProtoVania
{
  public interface IInventory
  {
    int inventorySize { get; }
    Dictionary<int, InventoryItem> itemList { get; }

    dynamic AddItem(InventoryItem inventoryItem) { return new { }; }
    dynamic AddItemToEmptySlot(InventoryItem inventoryItem) { return new { }; }
    void InsertStack(int index, InventoryItem inventoryItem) { }
    bool RemoveStack(int index) { return false; }
  }
}