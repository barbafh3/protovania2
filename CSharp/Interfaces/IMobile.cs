using Godot;
using System;

namespace ProtoVania
{
    public interface IMobile
    {
        virtual void MoveHorizontal(double delta) { }
        virtual void MoveVertical(double delta) { }
    }
}