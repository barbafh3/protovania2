using Godot;
using System;

public interface IPickable
{
  void GiveMaterial(Node2D body);
}
