namespace ProtoVania
{
  public interface IPowerSource
  {
    int powerCapacity { get; set; }
  }
}