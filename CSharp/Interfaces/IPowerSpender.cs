namespace ProtoVania
{
  public interface IPowerSpender
  {
    int PowerDrainage { get; set; }
    bool Powered { get; set; }
  }
}