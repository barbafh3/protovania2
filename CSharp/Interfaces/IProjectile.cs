namespace ProtoVania
{
  public interface IProjectile
  {
    EProjectileTypes damageType { get; set; }
  }
}