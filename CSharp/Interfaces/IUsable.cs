using Godot;
using System;

namespace ProtoVania
{
  public interface IUsable
  {
    public void Use(InventoryUI ui, Viewport root, PlayerController player);
  }
}