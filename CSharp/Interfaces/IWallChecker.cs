using Godot;
using System.Collections.Generic;

public interface IWallChecker
{
  bool IsTouchingWalls(RayCast2D leftWall, RayCast2D rightWall)
  {
    bool result = false;
    if (leftWall.IsColliding()) result = true;
    if (rightWall.IsColliding()) result = true;
    return result;
  }

  bool IsOnWallLedge(RayCast2D wall, RayCast2D ledge)
  {
    bool result = false;
    if (wall.IsColliding())
    {
      if (!ledge.IsColliding()) result = true;
      else result = false;
    }
    return result;
  }
}
