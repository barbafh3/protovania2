using Godot;
using System;

namespace ProtoVania
{
    public interface IPhysicsProcessState
    {
        void PhysicsProcess(double delta);
    }
}