using Godot;
using System;

namespace ProtoVania
{
    public interface IProcessState
    {
        void Process(double delta);
    }
}
