using System;
using Godot;

namespace ProtoVania
{
    public interface IState
    {
        String StateName { get; }

        void EnterState(Node2D controller);
        void ExitState();

    }
}
