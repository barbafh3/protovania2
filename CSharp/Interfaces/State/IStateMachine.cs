namespace ProtoVania
{
    public interface IStateMachine
    {
        IState CurrentState { get; }

        void ChangeState(IState state);
    }
}
