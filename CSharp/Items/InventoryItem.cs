using Godot;
using System;

namespace ProtoVania
{
  public class InventoryItem
  {
    public ItemData item { get; private set; }
    public int amount { get; private set; }

    public InventoryItem() { }

    public InventoryItem(ItemData item, int amount)
    {
      this.item = item;
      this.amount = amount;
    }

    public void SetItem(ItemData item) => this.item = item;

    public void SetAmount(int amount) => this.amount = amount;

    public void AddAmount(int amount) => this.amount += amount;

    public bool CompareTo(InventoryItem inventoryItem)
    {
      if (inventoryItem != null)
      {
        if (this.item.CompareTo(inventoryItem.item)
            && this.amount == inventoryItem.amount)
          return true;
        else return false;
      }
      else return false;
    }
  }
}