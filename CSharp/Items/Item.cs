using Godot;
using System;
using SC = System.Collections;
using GC = Godot.Collections;

namespace ProtoVania
{
    public partial class Item : Resource
    {
        public string Id { get; private set; }
        public String Name { get; private set; }
        public Texture Icon { get; private set; }
        public int StackLimit { get; private set; }
        public string AssetPath { get; private set; }
        public int BaseHealth { get; private set; } = 0;

        public Item()
        {
        }

        public Item(ItemData itemData, int baseHealth = 0)
        {
            Id = itemData.Id;
            Name = itemData.Name;
            Icon = itemData.Icon;
            StackLimit = itemData.StackLimit;
            AssetPath = itemData.AssetPath;
            BaseHealth = baseHealth;
        }

        public Item(string id, String name, Texture icon, int stackLimit, string assetPath, int baseHealth)
        {
            Id = id;
            Name = name;
            Icon = icon;
            StackLimit = stackLimit;
            AssetPath = assetPath;
            BaseHealth = baseHealth;
        }

        public void SetName(String name) => this.Name = name;

        public void SetIconPath(Texture icon) => this.Icon = icon;

        public void SetStackLimit(int stackLimit) => this.StackLimit = stackLimit;

        public void SetAssetPath(string assetPath) => this.AssetPath = assetPath;

        public void SetBaseHealth(int baseHealth) => this.BaseHealth = baseHealth;

        public bool CompareTo(Item item)
        {
            if (item != null)
            {
                if (this.Name == item.Name
                    && this.Icon == item.Icon
                    && this.StackLimit == item.StackLimit
                    && this.AssetPath == item.AssetPath
                    && this.BaseHealth == item.BaseHealth)
                    return true;
                else return false;
            }
            else return false;
        }
    }
}