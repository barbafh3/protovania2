using Godot;
using System.Collections.Generic;

namespace ProtoVania
{
  public partial class PowerGrid : Node2D
  {
    private static List<Node2D> _generators = new List<Node2D>();
    private static List<Node2D> _energySpenders = new List<Node2D>();
    private static float _powerBaseTick = 0.5f;
    private static int _capacity = 0;
    private static int _drainage = 0;
    private static double _powerTick = 0f;

    public override void _Process(double delta)
    {
      _DoPowerTick(delta);
      if (_powerTick <= 0)
      {
        _SumCapacities();
        _SumDrainage();
        _DistributeEnergy();
        _powerTick = _powerBaseTick;
      }
    }

    private static void _DoPowerTick(double delta)
    {
      if (_powerTick > 0)
        _powerTick -= delta;
    }

    private static void _SumCapacities()
    {
      _capacity = 0;
      if (_generators.Count > 0)
      {
        foreach (var generator in _generators)
        {
          if (generator != null)
            _capacity += ((IPowerSource)generator).powerCapacity;
        }
      }

    }

    private static void _SumDrainage()
    {
      _drainage = 0;
      if (_energySpenders.Count > 0)
      {
        foreach (var spender in _energySpenders)
        {
          if (spender != null)
            _drainage += ((IPowerSpender)spender).PowerDrainage;
        }
      }

    }

    private static void _DistributeEnergy()
    {
      Utils.ShuffleList(_energySpenders);
      foreach (IPowerSpender spender in _energySpenders)
      {
        if (spender.PowerDrainage <= _capacity)
        {
          _capacity -= spender.PowerDrainage;
          spender.Powered = true;
        }
        else
          spender.Powered = false;
      }
    }

    public static int GetTotalCapacity()
    {
      return _capacity;
    }

    public static int GetTotalDrainage()
    {
      return _drainage;
    }

    public static void Register(string type, Node2D controller)
    {
      if (type == "PowerSource")
      {
        if (!_generators.Contains(controller))
          _generators.Add(controller);
      }
      else
      {
        if (!_energySpenders.Contains(controller))
          _energySpenders.Add(controller);
      }
    }

    public static void Unregister(string type, Node2D controller)
    {
      if (type == "PowerSource")
      {
        if (_generators.Contains(controller))
          _generators.Remove(controller);
      }
      else
      {
        if (_energySpenders.Contains(controller))
          _energySpenders.Remove(controller);
      }
    }

  }
}