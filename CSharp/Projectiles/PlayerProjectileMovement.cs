using Godot;

namespace ProtoVania
{
    public partial class PlayerProjectileMovement : Area2D, IProjectile
    {
        [Export] public EProjectileTypes damageType { get; set; }

        [Export] public AudioStream ExplosionSound = null;
        AudioStreamPlayer2D _audioPlayer;

        [Export] public float Speed;
        [Export] public float ProjectileTimeout;
        [Export] public float ShakePower;
        [Export] public float ShakeDuration;
        [Export] public int Damage;

        Vector2 _mouseDirection;
        public PlayerController Player;
        AnimatedSprite2D _animatedSprite;

        Vector2 _velocity;

        bool _hasCollided = false;

        public override void _Ready()
        {
            InitializeVariables();
            ConnectSignals();
            _animatedSprite.Play("Movement");
            float angle = StartMoving();
            Flip(angle);
        }

        public override void _PhysicsProcess(double delta)
        {
            TimeoutTick((float)delta);
            if (ProjectileTimeout <= 0)
                QueueFree();
            if (!_hasCollided)
                Position += _velocity * (float)delta;
        }

        void TimeoutTick(float delta)
        {
            if (ProjectileTimeout > 0)
                ProjectileTimeout -= delta;
        }

        void InitializeVariables()
        {
            _animatedSprite = GetNode<AnimatedSprite2D>("AnimatedSprite");
            _audioPlayer = GetNode<AudioStreamPlayer2D>("AudioStreamPlayer2D");
        }

        void ConnectSignals()
        {
            BodyEntered += OnBodyCollision;
            AreaEntered += OnAreaCollision;
            _animatedSprite.AnimationFinished += DestroyProjectile;
        }

        float StartMoving()
        {
            var fireScript = Utils.GetNode<PlayerFire>(Player);
            float radAngle = GetAngleTo(GetGlobalMousePosition());
            float degAngle = Mathf.RadToDeg(radAngle);

            if (fireScript.ShotOriginOffset < 0)
            {
                if (degAngle < 90 && degAngle > 0)
                    degAngle = 90;
                if (degAngle <= -90 && degAngle > 180)
                    degAngle = -90;
            }
            else if (fireScript.ShotOriginOffset > 0)
            {
                if (degAngle > 90 && degAngle > 180)
                    degAngle = 90;
                if (degAngle <= -90 && degAngle >= 180)
                    degAngle = -90;
            }

            radAngle = Mathf.DegToRad(degAngle);
            _mouseDirection = new Vector2(Mathf.Cos(radAngle), Mathf.Sin(radAngle));
            _velocity = Speed * _mouseDirection;
            Rotation = radAngle;
            return radAngle;
        }

        void DestroyProjectile()
        {
            if (_animatedSprite.Animation == "Explode")
                QueueFree();
        }

        void OnAreaCollision(Node area)
        {
            if (area.IsInGroup("Enemy"))
            {
                Utils.GetNode<EnemyHealth>(area).TakeDamage(Damage);
                _hasCollided = true;
                CollisionEffects();
            }
        }

        void OnBodyCollision(Node body)
        {
            if (body.IsInGroup("Terrain"))
            {
                _hasCollided = true;
                CollisionEffects();
            }
        }

        void CollisionEffects()
        {
            GD.Print("Collision effects");
            SignalManager.Emit(this, SignalManager.SignalName.CameraShakeCalled, ShakePower, ShakeDuration);
            _animatedSprite.Play("Explode");
            _audioPlayer.Stream = ExplosionSound;
            _audioPlayer.Play();
        }

        void Flip(float angle)
        {
            var deg = Mathf.RadToDeg(angle);
            if (deg is < -90 and > -180 or > 90 and <= 180)
                GetNode<AnimatedSprite2D>("AnimatedSprite").FlipV = true;
        }
    }
}