using System.Collections.Generic;
using Godot;

namespace ProtoVania;

public static class RaycastUtils
{
    public static bool IsTouchingWalls(List<RayCast2D> walls)
    {
        var result = false;

        foreach (var wall in walls)
        {
            if (wall.IsColliding() 
                && wall.GetCollider() is Node collider 
                && collider.IsInGroup("Terrain")
            )
                result = true;
        }

        return result;
    }

    public static bool InOnWallLedge(RayCast2D wall, RayCast2D ledge)
    {
        var result = false;
        
        if (wall.IsColliding() 
            && wall.GetCollider() is Node wallCollider 
            && wallCollider.IsInGroup("Terrain"))
            if (!ledge.IsColliding())
                result = true;
            else if (ledge.GetCollider() is Node ledgeCollider // TODO: Is this else even needed?
                     && ledgeCollider.IsInGroup("Terrain"))
                result = false;

        return result;
    }

    public static bool CheckCompositeCollision(List<RayCast2D> rays, string group, Node2D ignoredNode)
    {
        var result = false;

        foreach (var ray in rays)
        {
            if (ray.IsColliding()
                && ray.GetCollider() is Node collider
                && collider != ignoredNode
                && collider.IsInGroup(group))
                result = true;
        }

        return result;
    }
}