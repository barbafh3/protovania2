using Godot;

namespace ProtoVania
{
    public partial class BuildingData : ItemData
    {
        [Export] public int BaseHealth;

        public void SpawnBuilding(InventoryUI ui, Viewport root)
        {
            var assetScene = ResourceLoader.Load(this.AssetPath) as PackedScene;
            var instance = assetScene.Instantiate();
            var script = instance as IBuilding;
            script.InventoryUi = ui;
            script.Id = this.Id;
            root.AddChild(instance);
            instance.GetNode<SignalManager>("/root/SignalManager").EmitSignal("CraftingStarted");
        }
    }
}