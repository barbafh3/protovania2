using Godot;

namespace ProtoVania
{
    public partial class CraftRecipeData : Resource
    {
        [Export] public string Id;
        [Export] public string Name;
        [Export] public ItemData CraftedItem;
        [Export] public float CraftingTime;
        [Export] public Godot.Collections.Dictionary<int, ItemData> Materials;
    }
}