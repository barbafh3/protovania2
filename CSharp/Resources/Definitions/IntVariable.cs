using Godot;

namespace ProtoVania
{
    public partial class IntVariable : Resource
    {
        [Export] public int value;
    }
}