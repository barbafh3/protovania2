using Godot;
using System;
using System.Collections.Generic;

namespace ProtoVania
{
    public partial class Inventory : Resource
    {
        [Signal]
        public delegate void ValuesChangedEventHandler();

        [Export] public int InventorySize;
        public Dictionary<int, InventoryItem> Values = new Dictionary<int, InventoryItem>();

        public dynamic AddItem(InventoryItem inventoryItem)
        {
            if (Values.Count < InventorySize)
            {
                List<int> matchingKeys = GetMatchingKeys(inventoryItem);
                if (matchingKeys.Count > 0)
                {
                    int overflow = 0;
                    foreach (var key in matchingKeys)
                    {
                        if (Values[key].amount >= inventoryItem.item.StackLimit)
                            continue;
                        
                        if (Values[key].amount + inventoryItem.amount > inventoryItem.item.StackLimit)
                        {
                            overflow = (Values[key].amount + inventoryItem.amount) - inventoryItem.item.StackLimit;
                            Values[key].AddAmount(inventoryItem.amount - overflow);
                            EmitSignal(SignalName.ValuesChanged);
                            return new { added = true, overflow = overflow };
                        }
                        
                        Values[key].AddAmount(inventoryItem.amount);
                        EmitSignal(nameof(ValuesChanged));
                        return new { added = true, overflow = overflow };
                    }

                    for (int i = 0; i < InventorySize; i++)
                    {
                        if (!Values.ContainsKey(i))
                        {
                            Values[i] = inventoryItem;
                            break;
                        }
                    }

                    EmitSignal(nameof(ValuesChanged));
                    return new { added = true, overflow = overflow };
                }
                else
                {
                    for (int i = 0; i < InventorySize; i++)
                    {
                        if (!Values.ContainsKey(i))
                        {
                            Values[i] = inventoryItem;
                            break;
                        }
                    }

                    EmitSignal(nameof(ValuesChanged));
                    return new { added = true, overflow = 0 };
                }
            }
            else
                return new { added = false, overflow = 0 };
        }

        public dynamic AddItemToEmptySlot(InventoryItem inventoryItem)
        {
            for (int i = 0; i < InventorySize; i++)
            {
                if (!Values.ContainsKey(i))
                {
                    Values[i] = inventoryItem;
                    EmitSignal(nameof(ValuesChanged));
                    return new { success = true, index = i };
                }
            }

            return new { success = false, index = -1 };
        }

        public bool RemoveStack(int index)
        {
            try
            {
                Values.Remove(index);
                EmitSignal(nameof(ValuesChanged));
                return true;
            }
            catch (Exception e)
            {
                GD.Print("PlayerInventory: Error = " + e);
                return false;
            }
        }

        public void InsertStack(int index, InventoryItem newItem)
        {
            if (Values.ContainsKey(index)) Values[index] = newItem;
            else Values.Add(index, newItem);
        }

        public void AddToStackAmount(int index, int amount) => Values[index].AddAmount(amount);

        public void SetStackAmount(int index, int amount) => Values[index].SetAmount(amount);

        public List<int> GetMatchingKeys(InventoryItem inventoryItem)
        {
            List<int> matchingKeys = new List<int>();
            if (Values.Keys.Count > 0)
            {
                foreach (var key in Values.Keys)
                {
                    if (Values[key].item.Name == inventoryItem.item.Name)
                        matchingKeys.Add(key);
                }
            }

            return matchingKeys;
        }
    }
}