using Godot;
using System;

namespace ProtoVania
{
    public partial class ItemData : Resource
    {
        [Export] public string Id;
        [Export] public String Name;
        [Export] public Texture2D Icon;
        [Export] public int StackLimit;
        [Export] public string AssetPath;

        public bool CompareTo(ItemData item)
        {
            if (item != null &&
                Name == item.Name &&
                Icon == item.Icon &&
                StackLimit == item.StackLimit &&
                AssetPath == item.AssetPath)
                return true;
            return false;
        }
    }
}