using Godot;
using System.Collections.Generic;

namespace ProtoVania
{
    public partial class ItemDatabase : Node
    {
        [Export] public Godot.Collections.Dictionary<string, Item> Items;
    }
}