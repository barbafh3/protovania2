using Godot;

namespace ProtoVania
{
    public partial class MaterialCostData : Resource
    {
        [Export] public int amount;
        [Export] public ItemData item;
    }
}