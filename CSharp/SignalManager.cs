using Godot;

namespace ProtoVania
{
    public partial class SignalManager : Node2D
    {
        [Signal] public delegate void CameraShakeCalledEventHandler(float power, float duration);

        [Signal] public delegate void HealthChangedEventHandler(int health);

        [Signal] public delegate void InventoryChangedEventHandler();
        [Signal] public delegate void InventoryOpenedEventHandler();

        [Signal] public delegate void InventoryClosedEventHandler();

        [Signal] public delegate void CraftingStartedEventHandler();

        [Signal] public delegate void CraftingEndedEventHandler();

        [Signal] public delegate void NewCraftSelectedEventHandler();

        [Signal] public delegate void SplitStackCalledEventHandler(StackInfo stack);

        [Signal] public delegate void SplitStackEndedEventHandler();

        [Signal] public delegate void ResetNodesEventHandler();

        public static void Emit(Node node, string name, params Variant[] args)
        {
            Utils.GetSignalManager(node).EmitSignal(name, args);
        }
    }
}