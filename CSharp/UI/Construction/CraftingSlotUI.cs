using Godot;
using System.Collections.Generic;

namespace ProtoVania
{
    public partial class CraftingSlotUI : Control
    {
        [Export] Texture2D _texture = null;
        [Export] public Inventory PlayerInventory;
        [Export] public CraftRecipeData Recipe;
        PackedScene _assetScene;
        TextureButton _button;
        Label _label;
        TextureRect _icon;

        private List<int> _keysToRemove = new List<int>();

        public override void _Ready()
        {
            InitializeVariables();
            ConnectSignals();
            OnInventoryChange();
        }

        #region Initialization

        void InitializeVariables()
        {
            _icon = GetNode("Button/ItemIcon") as TextureRect;
            _button = GetNode("Button") as TextureButton;
            _label = GetNode("Button/ItemCount") as Label;
            _icon.Texture = _texture;
            // player = GetTree().Root.GetNode("Player") as PlayerController;
            // if (playerInventory == null)
            //   player.SetInventory();
        }

        void ConnectSignals()
        {
            PlayerInventory.ValuesChanged += OnInventoryChange;
            Utils.GetSignalManager(this).SplitStackCalled += DisableSlot;
            Utils.GetSignalManager(this).SplitStackEnded += EnableSlot;
            _button.ButtonUp += OnButtonPressed;
        }

        #endregion

        #region UI

        void OnInventoryChange()
        {
            GD.Print("ValuesChanged");
            var units = CheckCraftableUnits();
            _label.Text = units.ToString();
            if (_label.Text == "0")
            {
                _label.Visible = false;
                _button.Disabled = true;
            }
            else
            {
                _label.Visible = true;
                _button.Disabled = false;
            }
        }

        int CheckCraftableUnits()
        {
            int units = -1;
            GD.Print(Recipe.Materials);
            foreach (var material in Recipe.Materials)
            {
                GD.Print(material.Value);
                var materialCount = CheckMaterialCount(material.Value.Id);
                var count = 0;
                if (materialCount != 0)
                    count = materialCount / material.Key;
                if (units == -1)
                    units = count;
                else if (count < units)
                    units = count;
            }

            if (units == -1) units = 0;
            return units;
        }

        int CheckMaterialCount(string id)
        {
            int materialCount = 0;
            foreach (var pair in PlayerInventory.Values)
            {
                if (pair.Value.item.Id == id)
                    materialCount += pair.Value.amount;
            }

            return materialCount;
        }

        void DisableSlot(StackInfo stack) => _button.Disabled = true;

        void EnableSlot() => _button.Disabled = false;

        #endregion

        #region Craft

        void OnButtonPressed()
        {
            if (TryCrafting())
            {
                var item = new InventoryItem(Recipe.CraftedItem, 1);
                PlayerInventory.AddItem(item);
            }
            else GD.Print("CraftingSlotUI: Failed to craft");
        }

        bool TryCrafting()
        {
            bool canCraft = false;
            bool crafted = false;
            if (PlayerInventory.Values.Count < PlayerInventory.InventorySize)
            {
                foreach (var material in Recipe.Materials)
                {
                    canCraft = IsMaterialEnough(material.Value.Id, material.Key);
                }

                if (canCraft)
                {
                    foreach (var material in Recipe.Materials)
                    {
                        crafted = SpendMaterial(material.Value.Id, material.Key);
                    }

                    if (crafted)
                    {
                        foreach (var key in _keysToRemove)
                        {
                            if (PlayerInventory.Values.ContainsKey(key))
                                PlayerInventory.Values.Remove(key);
                        }

                        PlayerInventory.EmitSignal(nameof(Inventory.ValuesChanged));
                        _keysToRemove = new List<int>();
                    }
                }
            }
            else GD.Print("CraftingSlotUI: Inventory is full");

            return crafted;
        }

        bool IsMaterialEnough(string id, int cost)
        {
            int materialTotal = 0;
            foreach (var pair in PlayerInventory.Values)
            {
                if (pair.Value.item.Id == id)
                {
                    materialTotal += pair.Value.amount;
                }
            }

            if (materialTotal >= cost) return true;
            else return false;
        }

        bool SpendMaterial(string id, int amount)
        {
            int remainingAmount = amount;
            bool succeeded = false;
            for (int i = 0; i < PlayerInventory.InventorySize; i++)
            {
                if (PlayerInventory.Values.ContainsKey(i))
                {
                    if (remainingAmount > 0)
                    {
                        if (PlayerInventory.Values[i].item.Id == id)
                        {
                            if (PlayerInventory.Values[i].amount > remainingAmount)
                            {
                                PlayerInventory.Values[i].AddAmount(-remainingAmount);
                                remainingAmount = 0;
                                succeeded = true;
                            }
                            else
                            {
                                remainingAmount -= PlayerInventory.Values[i].amount;
                                var inventoryAmount = PlayerInventory.Values[i].amount;
                                PlayerInventory.Values[i].SetAmount(0);
                                _keysToRemove.Add(i);
                                succeeded = true;
                            }
                        }
                    }
                    else succeeded = true;
                }
                else GD.Print("CraftingSlotUI: inventory does not have key " + i);
            }

            return succeeded;
        }

        #endregion
    }
}