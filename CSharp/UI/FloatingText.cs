using Godot;

public partial class FloatingText : Marker2D
{
    public string Text { get; private set; }
    public Texture2D Texture { get; private set; }

    public override void _Ready()
    {
        var label = GetNode<Label>("Label");
        var icon = GetNode<TextureRect>("TextureRect");
        label.Text = Text;
        icon.Texture = Texture;

        Tween tween = GetTree().CreateTween();
        tween
            .TweenProperty(
                this, 
                "modulate",
                new Color(Modulate.R, Modulate.G, Modulate.B, 0.0f),
                0.3f)
            .SetTrans(Tween.TransitionType.Linear)
            .SetEase(Tween.EaseType.Out);

    }

    public override void _Process(double delta)
    {
        Position = new Vector2(Position.X, Position.Y - (float)delta - 0.2f);
        if (Modulate.A <= 0.0f) QueueFree();
    }

    public void SetText(string text) => Text = text;

    public void SetIcon(Texture2D texture) => Texture = texture;
}