using Godot;
using LanguageExt;
using ProtoVania.CSharp.Characters.Player.Interfaces;

namespace ProtoVania;

public partial class AbilitySlot : TextureButton
{
    [Export] public string SkillName;
    [Export] public Texture2D SkillIcon;

    private Timer _timer;
    private TextureProgressBar _textureProgress;
    private TextureRect _icon;
    
    public Option<PlayerSkill> Skill = null;

    private PlayerController _player;

    private bool _initialized = false;

    public override void _Ready()
    {
        _timer = GetNode<Timer>("Timer");
        _textureProgress = GetNode<TextureProgressBar>("TextureProgress");
        _icon = GetNode<TextureRect>("Icon");
        SetSkillNode();
        Skill.IfSome(InitializeVariables);
    }

    public override void _Process(double delta)
    {
        Skill.IfSome(skill => _textureProgress.Value = (int)((_timer.TimeLeft / skill.BaseCooldown) * 100));
    }

    public override void _PhysicsProcess(double delta)
    {
        Skill.Match(
            Some: skill =>
            {
                if (!_initialized)
                    InitializeVariables(skill);
            },
            None: SetSkillNode);
    }

    void InitializeVariables(PlayerSkill skill)
    {
        skill.SkillUsed += OnSkillUsed;
        _timer.Timeout += OnTimeout;
        _timer.WaitTime = skill.BaseCooldown;
        _icon.Texture = SkillIcon;
        _textureProgress.TextureProgress = TextureDisabled;
        _textureProgress.Value = 0.0;
        _initialized = true;
        SetProcess(false);
    }

    void SetSkillNode()
    {
        var nodes = GetTree().GetNodesInGroup("PlayerSkill");
        foreach (var node in nodes)
        {
            if (node.Name == $"Player{SkillName}")
            {
                Skill = node as PlayerSkill;
                break;
            }
        }
    }

    void OnSkillUsed()
    {
        SetProcess(true);
        _timer.Start();
    }

    void OnTimeout()
    {
        _textureProgress.Value = 0;
        SetProcess(false);
    }
}