using Godot;
using System;

namespace ProtoVania
{
    public partial  class InventorySlotUI : Control
    {
        [Export] public int InventoryIndex = -1;
        StackInfo _stackInfo;
        TextureButton _button;
        InventoryUI _inventoryUi;

        public override void _Ready()
        {
            _button = GetNode<TextureButton>("Button");
            _inventoryUi = GetParent().GetParent() as InventoryUI;
            _stackInfo = GetNode<StackInfo>("Button/Stack");
            _stackInfo.InventoryIndex = InventoryIndex;
            Utils.GetSignalManager(this).SplitStackCalled += DisableSlot;
            Utils.GetSignalManager(this).SplitStackEnded += EnableSlot;
        }

        void DisableSlot(StackInfo stack)
        {
            _button.Disabled = true;
            _stackInfo.MouseFilter = MouseFilterEnum.Ignore;
        }

        void EnableSlot()
        {
            _button.Disabled = false;
            _stackInfo.MouseFilter = MouseFilterEnum.Pass;
        }
    }
}