using Godot;
using System.Collections.Generic;

namespace ProtoVania
{
  public partial class InventoryUI : Control
  {

    // public PlayerController player;
    [Export] public Inventory PlayerInventory;

    // public override void _EnterTree()
    // {
    //   player = GetParent().GetParent() as PlayerController;
    //   GD.Print(player.Name);
    // }

    public override void _Ready()
    {
      PlayerInventory.ValuesChanged += RefreshInventory;
      Utils.GetSignalManager(this).SplitStackCalled += SetDisabledColor;
      Utils.GetSignalManager(this).SplitStackEnded += RestoreColor;
    }

    public void SetVisibility(bool state) => Visible = state;

    void SetDisabledColor(StackInfo stack) => Modulate = new Color(0.34f, 0.34f, 0.34f, 1f);

    void RestoreColor() => Modulate = new Color(1f, 1f, 1f, 1f);

    void RefreshInventory()
    {
      for (int i = 0; i < PlayerInventory.InventorySize; i++)
      {
        var script = GetNode<StackInfo>("Slots/InventorySlot" + i + "/Button/Stack");
        var icon = GetNode<TextureRect>("Slots/InventorySlot" + i + "/Button/Stack/ItemIcon");
        var label = GetNode<Label>("Slots/InventorySlot" + i + "/Button/Stack/ItemCount");
        script.InventoryUi = this;
        if (PlayerInventory.Values.ContainsKey(i))
        {
          // icon.Texture = ResourceLoader.Load(playerInventory.values[i].item.icon) as Texture;
          icon.Texture = PlayerInventory.Values[i].item.Icon;
          label.Text = PlayerInventory.Values[i].amount.ToString();
          InventoryItem invItem = new InventoryItem(PlayerInventory.Values[i].item,
                                                    PlayerInventory.Values[i].amount);
          script.InventoryItem = invItem;
        }
        else
        {
          PlayerInventory.Values.Remove(i);
          script.InventoryItem = null;
          icon.Texture = null;
          label.Text = "0";
        }
      }
    }
  }
}