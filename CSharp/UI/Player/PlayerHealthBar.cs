using Godot;

namespace ProtoVania
{
  public partial class PlayerHealthBar : HBoxContainer
  {
    [Export] public IntVariable playerHealth;

    public override void _Process(double delta) => RefreshHealth(playerHealth.value);

    void RefreshHealth(int health) => GetNode<Label>("HealthAmountLabel").Text = health.ToString();

  }
}
