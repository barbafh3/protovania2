using Godot;

namespace ProtoVania
{
    public partial class StackInfo : Control
    {
        [Export] public Inventory PlayerInventory;
        public InventoryItem InventoryItem = null;
        public int InventoryIndex = -1;
        public InventoryUI InventoryUi;
        TextureButton _button;
        public Label Label;
        public TextureRect Icon;

        public override void _Ready()
        {
            _button = GetParent() as TextureButton;
            Label = GetNode<Label>("ItemCount");
            Icon = GetNode<TextureRect>("ItemIcon");
            _button.ButtonUp += OnButtonClicked;
            InventoryUi = Owner.Owner as InventoryUI;
        }

        public override void _Process(double delta)
        {
            if (Label.Text == "0") Label.Visible = false;
            else Label.Visible = true;
        }

        void OnButtonClicked()
        {
            if (Input.IsActionJustReleased("interact_ui"))
            {
                if (InventoryItem != null)
                {
                    if (InventoryItem.item is BuildingData)
                    {
                        InventoryItem.AddAmount(-1);
                        Label.Text = InventoryItem.amount.ToString();
                        ((BuildingData)InventoryItem.item).SpawnBuilding(InventoryUi, GetTree().Root);
                        PlayerInventory.Values[InventoryIndex].AddAmount(-1);
                        InventoryUi.SetVisibility(false);
                        SignalManager.Emit(this, SignalManager.SignalName.CraftingStarted);
                        if (InventoryItem.amount <= 0)
                        {
                            PlayerInventory.Values.Remove(InventoryIndex);
                            InventoryItem = null;
                            PlayerInventory.EmitSignal(nameof(Inventory.ValuesChanged));
                        }
                    }
                }
            }

            if (Input.IsActionJustReleased("cancel") && Input.IsKeyPressed(Key.Shift))
            {
                if (InventoryItem != null)
                    SignalManager.Emit(this, SignalManager.SignalName.SplitStackCalled, this);
            }
        }

        public object GetDragData(Vector2 position)
        {
            if (InventoryItem != null && MouseFilter != MouseFilterEnum.Ignore)
            {
                // var icon = ResourceLoader.Load(inventoryItem.item.icon) as Texture;
                var icon = InventoryItem.item.Icon;
                var rect = new TextureRect();
                rect.Texture = icon;
                rect.SelfModulate = new Color(1, 1, 1, 0.5f);
                rect.Scale = new Vector2(1.9f, 1.9f);
                SetDragPreview(rect);
                return this;
            }
            else return null;
        }

        public bool CanDropData(Vector2 position, object data)
        {
            if (data is StackInfo && data != this) return true;
            else return false;
        }

        public void DropData(Vector2 position, object data)
        {
            var dropingStack = data as StackInfo;
            if (dropingStack.InventoryItem != null)
            {
                var currentItem = InventoryItem;
                var newItem = dropingStack.InventoryItem;

                if (dropingStack.InventoryItem?.item.Id == InventoryItem?.item.Id)
                {
                    if (currentItem.amount < InventoryItem.item.StackLimit
                        && dropingStack.InventoryItem.amount < dropingStack.InventoryItem.item.StackLimit)
                    {
                        var amount = InventoryItem.amount + dropingStack.InventoryItem.amount;
                        if (amount <= InventoryItem.item.StackLimit)
                        {
                            InventoryItem.SetAmount(amount);
                            PlayerInventory.SetStackAmount(InventoryIndex, amount);
                            dropingStack.InventoryItem.SetAmount(0);
                            PlayerInventory.AddToStackAmount(dropingStack.InventoryIndex, -amount);
                            SignalManager.Emit(this, SignalManager.SignalName.InventoryChanged);
                            if (dropingStack.InventoryItem.amount <= 0)
                            {
                                PlayerInventory.RemoveStack(dropingStack.InventoryIndex);
                                dropingStack.InventoryItem = null;
                                SignalManager.Emit(this, SignalManager.SignalName.InventoryChanged);
                            }
                        }
                        else
                        {
                            var overflow = amount - InventoryItem.item.StackLimit;
                            InventoryItem.SetAmount(InventoryItem.item.StackLimit);
                            PlayerInventory.SetStackAmount(InventoryIndex, InventoryItem.item.StackLimit);
                            InventoryItem newStack = new InventoryItem(InventoryItem.item, overflow);
                            PlayerInventory.AddItem(newStack);
                            PlayerInventory.RemoveStack(dropingStack.InventoryIndex);
                            dropingStack.InventoryItem = null;
                            SignalManager.Emit(this, SignalManager.SignalName.InventoryChanged);
                        }
                    }
                    else
                    {
                        InventoryItem = newItem;
                        dropingStack.InventoryItem = currentItem;

                        PlayerInventory.InsertStack(dropingStack.InventoryIndex, currentItem);
                        PlayerInventory.InsertStack(InventoryIndex, newItem);

                        if (dropingStack.InventoryItem == null)
                            PlayerInventory.RemoveStack(dropingStack.InventoryIndex);
                        if (InventoryItem == null) PlayerInventory.RemoveStack(InventoryIndex);

                        SignalManager.Emit(this, SignalManager.SignalName.InventoryChanged);
                    }
                }
                else
                {
                    InventoryItem = newItem;
                    dropingStack.InventoryItem = currentItem;

                    PlayerInventory.InsertStack(dropingStack.InventoryIndex, currentItem);
                    PlayerInventory.InsertStack(InventoryIndex, newItem);

                    if (dropingStack.InventoryItem == null) PlayerInventory.RemoveStack(dropingStack.InventoryIndex);
                    if (InventoryItem == null) PlayerInventory.RemoveStack(InventoryIndex);

                    PlayerInventory.EmitSignal(nameof(Inventory.ValuesChanged));
                }
            }
        }
    }
}