using Godot;

namespace ProtoVania
{
    public partial class StackSplit : Control
    {
        [Export] public Inventory PlayerInventory;

        TextureButton _inputButton;
        TextureButton _outputButton;
        Button _moreLeftButton;
        Button _evenMoreLeftButton;
        Button _moreRightButton;
        Button _evenMoreRightButton;
        Button _splitButton;

        TextureRect _inputIcon;
        Label _inputLabel;
        TextureRect _outputIcon;
        Label _outputLabel;

        StackInfo _inputStack;
        int _stackIndex;
        InventoryItem _inputItem;
        InventoryItem _outputItem;

        public override void _Ready()
        {
            InitializeVariables();
            ConnectSignals();
            SetMouseInteraction(false);
            Visible = false;
        }

        public override void _Process(double delta)
        {
            if (Input.IsKeyPressed(Key.Escape))
                SignalManager.Emit(this, SignalManager.SignalName.SplitStackEnded);
        }

        void InitializeVariables()
        {
            _inputButton = GetNode("Input") as TextureButton;
            _outputButton = GetNode("Output") as TextureButton;
            _moreLeftButton = GetNode("MoreLeft") as Button;
            _evenMoreLeftButton = GetNode("EvenMoreLeft") as Button;
            _moreRightButton = GetNode("MoreRight") as Button;
            _evenMoreRightButton = GetNode("EvenMoreRight") as Button;
            _splitButton = GetNode("Split") as Button;
            _inputIcon = GetNode("Input/ItemIcon") as TextureRect;
            _inputLabel = GetNode("Input/ItemCount") as Label;
            _outputIcon = GetNode("Output/ItemIcon") as TextureRect;
            _outputLabel = GetNode("Output/ItemCount") as Label;
        }

        void ConnectSignals()
        {
            Utils.GetSignalManager(this).InventoryClosed += OnSplitStackEnd;
            Utils.GetSignalManager(this).SplitStackCalled += OnSplitStackCall;
            Utils.GetSignalManager(this).SplitStackEnded += OnSplitStackEnd;
            _splitButton.ButtonUp += OnSplitPressed;
            _moreLeftButton.ButtonUp += OnMoreLeftPressed;
            _evenMoreLeftButton.ButtonUp += OnEvenMoreLeftPressed;
            _moreRightButton.ButtonUp += OnMoreRightPressed;
            _evenMoreRightButton.ButtonUp += OnEvenMoreRightPressed;
        }

        public void SetMouseInteraction(bool enable)
        {
            if (enable)
            {
                MouseFilter = MouseFilterEnum.Stop;
                _inputButton.MouseFilter = MouseFilterEnum.Stop;
                _outputButton.MouseFilter = MouseFilterEnum.Stop;
                _moreLeftButton.MouseFilter = MouseFilterEnum.Stop;
                _evenMoreLeftButton.MouseFilter = MouseFilterEnum.Stop;
                _moreRightButton.MouseFilter = MouseFilterEnum.Stop;
                _evenMoreRightButton.MouseFilter = MouseFilterEnum.Stop;
                _splitButton.MouseFilter = MouseFilterEnum.Stop;
            }
            else
            {
                MouseFilter = MouseFilterEnum.Ignore;
                _inputButton.MouseFilter = MouseFilterEnum.Ignore;
                _outputButton.MouseFilter = MouseFilterEnum.Ignore;
                _moreLeftButton.MouseFilter = MouseFilterEnum.Ignore;
                _evenMoreLeftButton.MouseFilter = MouseFilterEnum.Ignore;
                _moreRightButton.MouseFilter = MouseFilterEnum.Ignore;
                _evenMoreRightButton.MouseFilter = MouseFilterEnum.Ignore;
                _splitButton.MouseFilter = MouseFilterEnum.Ignore;
            }
        }

        void OnSplitStackCall(StackInfo newStack)
        {
            Visible = true;
            SetMouseInteraction(true);

            _inputStack = newStack;
            _stackIndex = newStack.InventoryIndex;
            if (_inputStack.InventoryItem != null)
            {
                _inputItem = new InventoryItem();
                _inputItem.SetItem(_inputStack.InventoryItem.item);
                _inputItem.SetAmount(_inputStack.InventoryItem.amount);
                // inputIcon.Texture = ResourceLoader.Load(inputItem.item.icon) as Texture;
                _inputIcon.Texture = _inputItem.item.Icon;
                _inputLabel.Text = _inputItem.amount.ToString();
                // outputIcon.Texture = ResourceLoader.Load(inputItem.item.icon) as Texture;
                _outputIcon.Texture = _inputItem.item.Icon;
                _outputItem = new InventoryItem();
                _outputItem.SetItem(_inputItem.item);
            }
        }

        void OnMoreLeftPressed()
        {
            if (_inputItem.amount < _inputStack.InventoryItem.amount)
            {
                _inputItem.AddAmount(1);
                _inputLabel.Text = _inputItem.amount.ToString();
                _outputItem.AddAmount(-1);
                _outputLabel.Text = _outputItem.amount.ToString();
            }
        }

        void OnEvenMoreLeftPressed()
        {
            if (_inputItem.amount + 5 <= _inputStack.InventoryItem.amount)
            {
                _inputItem.AddAmount(5);
                _inputLabel.Text = _inputItem.amount.ToString();
                _outputItem.AddAmount(-5);
                _outputLabel.Text = _outputItem.amount.ToString();
            }
            else
            {
                var difference = _inputStack.InventoryItem.amount - _inputItem.amount;
                _inputItem.AddAmount(difference);
                _inputLabel.Text = _inputItem.amount.ToString();
                _outputItem.AddAmount(-difference);
                _outputLabel.Text = _outputItem.amount.ToString();
            }
        }

        void OnMoreRightPressed()
        {
            if (_outputItem.amount < _inputStack.InventoryItem.amount)
            {
                _inputItem.AddAmount(-1);
                _inputLabel.Text = _inputItem.amount.ToString();
                _outputItem.AddAmount(1);
                _outputLabel.Text = _outputItem.amount.ToString();
            }
        }

        void OnEvenMoreRightPressed()
        {
            if (_outputItem.amount + 5 <= _inputStack.InventoryItem.amount)
            {
                _inputItem.AddAmount(-5);
                _inputLabel.Text = _inputItem.amount.ToString();
                _outputItem.AddAmount(5);
                _outputLabel.Text = _outputItem.amount.ToString();
            }
            else
            {
                var difference = _inputStack.InventoryItem.amount - _outputItem.amount;
                _inputItem.AddAmount(-difference);
                _inputLabel.Text = _inputItem.amount.ToString();
                _outputItem.AddAmount(difference);
                _outputLabel.Text = _outputItem.amount.ToString();
            }
        }

        void OnSplitPressed()
        {
            if (_outputItem.amount > 0)
            {
                var operation = PlayerInventory.AddItemToEmptySlot(_outputItem);
                if (operation.success)
                {
                    PlayerInventory.InsertStack(_stackIndex, _inputItem);
                    SignalManager.Emit(this, SignalManager.SignalName.InventoryChanged);
                    SignalManager.Emit(this, SignalManager.SignalName.SplitStackEnded);
                }
                else
                {
                    // TODO: Show proper inventory full message
                    GD.Print("StackSplit: Inventory full");
                }
            }
            else
                SignalManager.Emit(this, SignalManager.SignalName.SplitStackEnded);
        }

        void OnSplitStackEnd()
        {
            Visible = false;
            SetMouseInteraction(false);
            ResetFields();
        }

        void ResetFields()
        {
            _inputIcon.Texture = null;
            _inputLabel.Text = "0";
            _outputIcon.Texture = null;
            _outputLabel.Text = "0";
            _inputStack = null;
            _inputItem = null;
            _outputItem = null;
        }
    }
}