using Godot;
using System;

namespace ProtoVania
{
    public partial class TrashCan : TextureButton
    {
        [Export] public Inventory PlayerInventory;

        public override void _Ready()
        {
            Utils.GetSignalManager(this).SplitStackCalled += DisableSlot;
            Utils.GetSignalManager(this).SplitStackEnded += EnableSlot;
        }

        void DisableSlot(StackInfo stack) => Disabled = true;

        void EnableSlot() => Disabled = false;

        public bool CanDropData(Vector2 position, object data)
        {
            if (data is StackInfo) return true;
            else return false;
        }

        public void DropData(Vector2 position, object data)
        {
            GD.Print("TrashCan: item dropped");
            var stackInfo = data as StackInfo;
            if (stackInfo.InventoryItem != null)
            {
                GD.Print("TrashCan: removing item");
                PlayerInventory.RemoveStack(stackInfo.InventoryIndex);
                stackInfo.InventoryItem = null;
                PlayerInventory.EmitSignal(nameof(Inventory.ValuesChanged));
            }
        }
    }
}