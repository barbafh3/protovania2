using Godot;
using System;
using System.Collections.Generic;
using ProtoVania;

public partial class Utils : Node
{
    /// <summary>
    /// Shuffles a list of objects of type <b>T</b>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="objList"></param>
    public static void ShuffleList<T>(IList<T> objList)
    {
        Random rnd = new Random();
        int totalItem = objList.Count;
        T obj;
        while (totalItem >= 1)
        {
            totalItem -= 1;
            int nextIndex = rnd.Next(totalItem, objList.Count);
            obj = objList[nextIndex];
            objList[nextIndex] = objList[totalItem];
            objList[totalItem] = obj;
        }
    }

    /// <summary>
    /// Returns a list of direct children of <b>parent<i/> with the specified <b>T</b> type.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="parent">Parent object</param>
    /// <returns>List of child nodes, if any.</returns>
    public static List<T> GetNodes<T>(Node2D parent) where T : class
    {
        var children = parent.GetChildren();
        List<T> childrenWithType = new List<T>();
        foreach (Node2D child in children)
        {
            if (child is T)
                childrenWithType.Add(child as T);
        }
        return childrenWithType;
    }

    /// <summary>
    /// Returns the first direct child of <b>parent</b> with the specified <b>T</b> type.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="parent">Parent object</param>
    /// <returns>Returns <b>null</b> if no child of type <b>T</b> is found.</returns>
    public static T GetNode<T>(Node parent) where T : class
    {
        var children = parent.GetChildren();
        List<T> childrenWithType = new List<T>();
        foreach (var child in children)
        {
            if (child is T)
                childrenWithType.Add(child as T);
        }
        if (childrenWithType.Count > 0)
            return childrenWithType[0];
        else return null;
    }

    /// <summary>
    /// Converts <b>double</b> value to <b>float</b>
    /// </summary>
    /// <param name="value">Input <b>double</b> value</param>
    /// <returns><b>float</b> value</returns>
    public static float ToSingle(double value) => (float)value;

    public static SignalManager GetSignalManager(Node node)
    { 
        var signalManager = node.GetNode<SignalManager>("/root/SignalManager");
        return signalManager;
    }
}
