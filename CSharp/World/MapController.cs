using Godot;

namespace ProtoVania
{
    public partial class MapController : Node2D
    {
        public override void _Ready()
        {
            dynamic firstDynamicUse = new { dynamicUsed = true };
            GD.Print("Dynamic type loaded: " + firstDynamicUse.dynamicUsed.ToString());
            var platforms = GetNode<TileMap>("Terrain/Platforms");
            GetNode<PlayerController>("Characters/Player").Platforms = platforms;
        }
    }
}
