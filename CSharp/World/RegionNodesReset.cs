using Godot;
using System;

namespace ProtoVania
{
    public partial class RegionNodesReset : Area2D
    {
        public override void _Ready() => BodyEntered += OnBodyEntered;

        void OnBodyEntered(Node2D body)
        {
            if (body.IsInGroup("Player")) SignalManager.Emit(this, SignalManager.SignalName.ResetNodes);
        }

    }
}
